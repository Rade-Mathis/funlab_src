from PIL import Image
import numpy as np
import tensorflow as tf

import preprocess as pp

x = tf.placeholder (tf.float32, [1, 150, 150, 3])
# xpp = pp._global_contrast_normalization (pp._bad_lcn (x))
xpp = pp._bad_lcn (x)

#name = '/home/rademathis/work/datasets/kather/small/train/01_TUMOR/1EAE_CRC-Prim-HE-10_029.tif_Row_1_Col_451.tif'
name = '/home/rademathis/work/datasets/kather/small/train/01_TUMOR/1F07_CRC-Prim-HE-07_025.tif_Row_1651_Col_1.tif'
img = np.array (Image.open (name))

init_op = tf.global_variables_initializer ()
session = tf.Session ()
session.run (init_op)

img_pp = session.run (xpp, feed_dict={x:[img]})[0]

minimum = img_pp.min ()
img_pos = img_pp - minimum
maximum = img_pos.max ()
print (minimum, maximum)
img_boost = (img_pos * 255) / maximum

#print (img_boost)
Image.fromarray (img).show ()
Image.fromarray (img_boost.astype (np.uint8)).show ()
