"""
The purpose of this file is to apply a -real- Local Contrast Normalization on
an image.
This will mainly be sequential, hence I recommand to parallelise it in lauching
it several times on separated folders.

nb : there is no padding
"""
from PIL import Image
import numpy as np
import os
import progress
import sys


def lcn_folder (in_dir :str, out_dir :str, window_size=7, kernel="linear",
                per_color=False) :
    """ 
    Make sure all file in the in_dir are images 
    Don't put any '/' at the end of you dirs' name
    """
    file_list = os.listdir (in_dir)
    ii = 0
    for i in file_list :
        write_lcn_img (in_dir +'/'+ i, out_dir +'/'+ i, window_size, kernel)
        progress.clear_line ()
        progress.print_percent (ii / len (file_list),
                                "[LCN] {}".format (in_dir))
        ii += 1
    print ()        


def write_lcn_img (in_file, out_file, window_size=7, kernel="linear",
                   per_color=False) :
    img = lcn_img (in_file, window_size, kernel, per_color)
    img -= img.min ()
    img /= img.max ()
    img *= 255
    Image.fromarray (img.astype (np.uint8)).save (out_file, "TIFF")

def lcn_img (filename, window_size=7, kernel_type="linear", per_color=False) :
    """ TODO : make it with a gaussian kernel """
    # Set the matrices
    in_image = np.array (Image.open (filename))
    shape = in_image.shape
    if per_color :
        mean_mat   = np.empty ((shape[0], shape[1], 3))  # see below
        stddev_mat = np.empty ((shape[0], shape[1], 3))  # see below
    else :
        mean_mat   = np.empty (shape)  # The local means
        stddev_mat = np.empty (shape)  # The local adjusted standart deviations

    # Check window size
    if window_size % 2 == 0 :
        raise ValueError ("window_size has to be odd")

    # Set a kernel
    kernel = np.empty ((window_size, window_size))
    if kernel_type == "const" :
        kernel.fill (1.)
    elif kernel_type == "linear" :
        for i in range (window_size) :
            for j in range (window_size) :
                center = window_size // 2
                dist = ((i - center) ** 2 + (j - center) ** 2) ** .5
                if dist > center :
                    kernel[i,j] = 0
                else :
                    kernel[i,j] = 1 - (dist / center)
        kernel /= kernel.mean ()
    else :
        raise NotImplemented

    # Get new values
    radius = window_size // 2
    X, Y, _ = shape
    for i in range (X) :
        for j in range (Y) :
            # Get the input window borders
            right  = min (i + radius + 1, X)  # excluded
            top    = max (j - radius,     0)  # included
            left   = max (i - radius,     0)  # included
            bottom = min (j + radius + 1, Y)  # excluded

            # Get the kernel borders
            to_right  = right - i
            to_top    = j - top
            to_left   = i - left
            to_bottom = bottom - j
            k_right  = radius + to_right
            k_top    = radius - to_top
            k_left   = radius - to_left
            k_bottom = radius + to_bottom

            # Compute locals means and stddev
            in_window = in_image [left:right, top:bottom]
            croped_kernel = kernel [k_left:k_right, k_top:k_bottom]
            mean, stddev = _patch_lcn (in_window, croped_kernel, per_color)
            stddev = max (stddev, 1 / max (X, Y))  # Prevents it from being 0
            mean_mat[i,j] = mean
            stddev_mat[i,j] = stddev

    # Apply the contrast notmalization
    if per_color :
        out_image = np.empty (mean.shape)
        for i in range (3) :
            out_image[:,:,i] = (in_image[:,:,i] - mean_mat[i]) / stddev_mat[i]
        return out_image
    else :
        return (in_image - mean_mat) / stddev_mat
                    
            
def _patch_lcn (image, kernel, per_color=False) :
    exp_kernel = np.empty ((kernel.shape[0], kernel.shape[1], 3))
    for i in range (3) :
        exp_kernel[:,:,i] = kernel
    kernel = exp_kernel

    if per_color :
        mean = (image * kernel).mean (axis=0).mean (axis=0)
        stddev = np.empty (3)
        for i in range (3) :
            stddev[i] = (((image[:,:,i] - mean[i]) ** 2) * kernel[:,:,i])     \
                        .mean ()
    else :    
        mean = (image * kernel).mean ()    
        stddev = (((image - mean) ** 2) * kernel).mean ()
        
    return mean, stddev


if __name__ == "__main__" :
    if len (sys.argv) < 5 :
        print ("{} <input-dir> <output-dir> <window-size> <kernel-type> "
               "<use per-color?>".format (sys.argv[0]))
        exit (1)
    lcn_folder (sys.argv[1], sys.argv[2], int (sys.argv[3]), sys.argv[4],
                bool (sys.argv[5]))
    
