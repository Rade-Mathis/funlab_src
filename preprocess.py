""" 
A little lib to preprocess my data.
Initially created as a kather_cnn_1.py dependency.

TODO : think of parallel_iterations=8  # 8 may not be the best value , tesit
"""

import math
import numpy as np
import tensorflow as tf

# File constants
VERSION_MAJOR = 0
VERSION_MINOR = 3
VERSION_MICRO = 1
VERSION_LABEL = "plcn::gaussian_alpha"

def preprocesses (in_data, preprocesses_list, name="preprocesses") :
    """
    Apply a brunch of specifed preprocesses to an image data.

    Parameters
    ----------
    in_data : a 4-tensorflow.tensor (batch, height, width, channels)
        The data to pre-process
    preprocesses_list : a list of string
        The names of the preprocesses to apply to in_data
    name : string, optional
        Used as a suffix to label the TensorFlow nodes

    Returns
    -------
    A 4-tensorflow.tensor of the same dimensions as in_data : the preprocessed
    data.

    Note
    ----
    The order of the processes names in preprocesses_list matters: this 
    function will pipeline the preprocess in the same order as given in the 
    list.
    """
    for i in preprocesses_list :
        if i in ["raw", None, "None"] :
            continue
        in_data = _preprocess (in_data, i, name=name + "_" + str(i))
    return in_data


def _preprocess (in_data, process_name, name) :
    """
    Apply a specifed preprocess to an image data.

    Parameters
    ----------
    in_data : a 4-tensorflow.tensor (batch, height, width, channels)
        The data to pre-process
    process_name : string
        The name of the preprocess to apply to in_data
    name : string
        Used as a suffix to label the TensorFlow nodes

    Returns
    -------
    A 4-tensorflow.tensor of the same dimensions as in_data : the preprocessed
    data.
    """
    if process_name == "gcn" :
        return _global_contrast_normalization (in_data)
    elif process_name == "lcn" :
        return _local_contrast_normalization (in_data)
    else :
        raise ValueError ("unknown preprocess name : {}".format (process_name))


def _global_contrast_normalization (in_data) :
    return tf.map_fn (lambda img : tf.image.per_image_standardization (img),
                      in_data, parallel_iterations=8)


def _local_contrast_normalization (in_data, window_size=5, name="lcn") :
    return _bad_lcn (in_data, window_size, "linear")


def _good_lcn (in_data, window_size=5, name="lcn") :
    raise NotImplemented ("I don't know how to code it well yet")
    
    
def _bad_lcn (in_data, window_size=5, kernel="linear", name="blcn") :
    """
    We are here trying to implement LeCun et al.'s LCN, based on the 
    mathematical formula of tensorflow.image.per_image_standardization :
    $$ (x - mean) / max( standart-deviation, 1 / sqrt( nbPixels ) ) $$

    This implementation is based on the use of several tf.nn.conv2d, I don't if
    it is the best or even a good idea, but since I am a beginer un TensorFlow,
    this is the only way I found.

    DISCLAIMER : This is implemented as findable in the internet literature,
    and in ``What is the Best Multi-Stage Architecture for Object Recognition?
    Kevin Jarrett, Koray Kavukcuoglu, Marc’Aurelio Ranzato and Yann LeCun''. I
    really think their implementation is wrong in the sense that it is not a
    real local normalization. But they got results and I suppose this
    implementation should be faster than the real one ... so why not ...

    TODO : replace the full-1 constant by a gaussian filter
    """
    # Get the number of pixels by local window
    nb_pixels = window_size ** 2

    # Get a kernel
    if kernel == "linear" :
        w_sum = tf.constant (1., shape=[window_size, window_size, 3, 3])
    elif kernel == "gaussian" :
        w_sum = _create_gaussian_window (window_size)

    # Get the sum of all local intensities
    i_sum = tf.nn.conv2d (in_data, w_sum, [1,1,1,1], padding="SAME",
                          name=name+"_reduce-sum")
    # Get the mean
    mean = i_sum / nb_pixels

    # Center the data
    centered = in_data - mean
    
    # Get the pseudo-standart-deviation
    centered2 = centered ** 2
    c_sum = tf.nn.conv2d (centered2, w_sum, [1,1,1,1], padding="SAME",
                          name=name+"reduce-sum")
    pstddev = tf.sqrt (c_sum)

    # Get the adjusted stddev
    astddev = tf.maximum (pstddev, 1 / (nb_pixels ** .5))

    # Calculate the local standardization
    return (in_data - mean) / astddev


def _gaussienne (x, mean=0, stddev=1) :
    return math.exp (- ((x - mean) ** 2) / (2 * (stddev ** 2))) \
        / stddev * math.sqrt (2 * math.pi)


def _create_gaussian_window (dim) :
    """ C'est sequentiel, donc plutot de la merde """
    w = np.empty ((dim, dim))
    for i in range (dim) :
        for j in range (dim) :
            mono = math.sqrt (i**2 + j**2)
            w[i][j] = _gaussienne (mono)
    w /= w.sum ()
    
            
