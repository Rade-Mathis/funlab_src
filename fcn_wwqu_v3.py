"""
Ici on va tenter de faire un FCN qui trouve les glandes du dataset Warwick-QU,
Comme le challenge GLAS

TODO :
- principal
  -
- optional
  - 5x5 windows are nice, but what about 7x7 ? Or more ?
  - augment dataset by rotating pictures by 90, 180 and 270 degrees
"""
# Computation imports
import numpy as np
import tensorflow as tf

# Image treatement imports
from PIL import Image

# Local imports
import arg_lexer
import layers
from Wwqu_Loader import Wwqu_Loader
import wwqu_networks

# Common util imports
import argparse
import os
import random
import sys
import progress

# File version
VERSION_MAJOR = 0
VERSION_MINOR = 6
VERSION_MICRO = 0
VERSION_LABEL = "unbalanced_loss"

NB_TRAIN = 85  # from 1 to 85
NB_TEST  = 60  # from 1 to 60
    

def loss (logits, labels) :
    """ Kinda obsolete temporary stuff. Should be cleaned soon. """
    return my_loss (logits, labels, .5)

def my_loss (logits, labels, label_1_weight=.5) :
    """ 
    Calculate a weighted cross entropy between logits and labels

    Parameters
    ----------
    logits : 4D-Tensor
        The logits predicted by the network
    labels : 4D-Tensor
        The given label. As to be of the same shape as `logits`.
    label_1_weight : float, optional (.5)
        A number between 0 and 1 : how much should be prioritize the second
        label on the first one ?
        If it worths 0.5, then it will just compute a classical cross entropy.
        The bigger it is, the more loss will be given to a label 1 classifed as
        a 0 and the less loss will be given to a label 0 classified as a 1.
        You can also set it as "auto", then the value will be decided to be the
        proportion of label 1 in the batch of `labels`.
  
    Retuns
    ------
    A float : the computed loss of the batch.

    Todo
    ----
    Dynamiser le truc au'on puisse choisir le nombre de classes
    """
    with tf.name_scope('loss'):
        if label_1_weight == "auto" :
            raise NotImplemented
        elif type (label_1_weight) is not float :
            raise ValueError ("label_1_weight should a float")
        elif not 0 <= label_1_weight <= 1 :
            raise ValueError ("label_1_weight should be in [0,1]")
        
        num_classes = 2
        balance = 2 * label_1_weight
        
        flat_logits = tf.reshape(logits, (-1, num_classes))
        flat_labels = tf.to_float(tf.reshape(labels, (-1, num_classes)))
        diff = flat_labels - flat_logits

        entropy_0 = tf.abs ((balance - (tf.sign (diff[:,1]) + 1)) * diff[:,1])
        entropy_1 = tf.abs ((balance +  tf.sign (diff[:,0]) - 1 ) * diff[:,0])
        x_entropy = entropy_0 + entropy_1
        mean_x_entropy = tf.reduce_mean (x_entropy)
    return mean_x_entropy    


def get_learning_rate (eta, iteration) :
    """ 
    Get the present wanted learning rate, at a given iteration

    Parameters
    ----------
    eta : list
        * if eta[0] is a float  -> fill always return that
        * if eta[0] is "linear" -> return eta[2] / (eta[1] * it)
        * if eta[0] is "pow"    -> return eta[1] / it^2
    it :
        The current iteration

    Returns
    -------
    A float : the required learning rate
    """
    try :
        return float (eta[0])
    except :
        it = iteration + 1  # prevent division by 0
        if eta[0] == "linear" :
            try :
                if len (eta) < 2 :
                    return .001 / it
                elif len (eta) == 2 :
                    return .001 - float (eta[1]) * it
                else :
                    return float (eta[2]) - float (eta[1]) * it
            except :
                raise ValueError ("If given 2nd and/or 3rd value to eta, they "
                                  "should be floats")
        elif eta[0] == "pow" :
            try :
                if len (eta) == 1 :
                    return .001 / it**2
                elif len (eta) == 2 :
                    return .001 / (it ** float (eta[1]))
                else :
                    return float (eta[2]) / (it ** float (eta[1]))
            except :
                raise ValueError ("If given 2nd and/or 3rd value to eta, they "
                                  "should be floats")
        else :
            raise ValueError (
                "First eta value should be a number or \"linear\" or "
                "\"quadratic\"")

        
def get_topology (name :str) :
    """ 
    Given the name of the network, returns the constructing function 

    Parameter
    ---------
    name : str
        Should be looking like "aC_bT_cP". a, b, and c reprensenting ints
        greaters that 0.

    Returns
    -------
    Will return a 3-tuple<int> (a,b,c), extracted from the `name` string.
    """
    def raise_usage () :
        "Internal code factorisation"
        raise ValueError (
            "topology string should be as \"xC_yT_zP\" with x,y,z ints > 0")
    
    try :
        c, t, p = name.split ('_')
    except :
        raise_usage ()
    c = c.replace ('C', '')
    t = t.replace ('T', '')
    p = p.replace ('P', '')

    try :
        c = int (c)
        t = int (t)
        p = int (p)
    except :
        raise_usage ()
    return c, t, p  # And not tcp, we are not in this kind of networks ...
    

def get_f1_score (prediction, y_bin) :
    """ Simple subroutine to compute the F-score from a predition """
    px_wise_success = tf.cast (tf.equal (prediction, y_bin), tf.int64)
    px_wise_failure = tf.negative (px_wise_success - 1)
    negatives = tf.negative (prediction - 1)
    true_pos_x2 = tf.reduce_sum (tf.multiply (prediction, px_wise_success)) * 2
    false_pos   = tf.reduce_sum (tf.multiply (prediction, px_wise_failure))
    false_neg   = tf.reduce_sum (tf.multiply (negatives,  px_wise_failure))
    return tf.cond (tf.equal (true_pos_x2, 0),
                    lambda : tf.cast (0., tf.float64),
                    lambda : tf.divide (
                        true_pos_x2,
                        true_pos_x2 + false_pos + false_neg))


def experiement_setting (nb_labels, topology, pool_fn, pool_size, pool_args) :
    """
    Sunroutine setting all the TF operation of getting the data, setting the 
    optimizer and stuff.

    Parameters
    ----------
    nb_labels : int
        The number of different labels, usualy 1, can be 2 if you use both 
        normal and borders label.
    topology : 3-tuple<int>
        The number of (conv_layers, transconv_layers, postconv_layers)
    pool_fn : function
        The function use as pooling layers.
    pool_size : int
        The shape of the pooling window will be [1, pool_size, pool_size, 1]
    pool_args :
        The argument may be given to the pool_fn. Set it as None if no argument
        us needed.

    Returns
    -------
    A tuple full of tf operators
    """
    # Load data
    x = tf.placeholder (tf.float32, [None, 416, 416, 3], name="x")
    y = tf.placeholder (tf.float32, [None, 416, 416, 1], name="y")
    if nb_labels == 2 :
        yb = tf.placeholder (tf.float32, [None, 416, 416, 1], name="yb")

    # Compute answer
    x_gcn = tf.map_fn (lambda img : tf.image.per_image_standardization (img),
                       x, parallel_iterations=8)
    logits = wwqu_networks.create_network (
        x_gcn, nb_labels, topology[0], topology[1], topology[2],
        pool_fn=pool_fn, pool_shapes=pool_size, pool_args=pool_args,
        postconv_shapes=3)
    
    # Shape label as wanted
    y_rshp = tf.reshape (y, [-1, 416, 416])
    y_bin = tf.cast (y_rshp > .5, tf.int64)  # [0;1] -> {0;1}
    y_1hot = tf.one_hot (y_bin, 2)
    if nb_labels == 2 :
        yb_rshp = tf.reshape (yb, [-1, 416, 416])
        yb_bin = tf.cast (yb_rshp > .5, tf.int64)  # [0;1] -> {0;1}
        yb_1hot = tf.one_hot (yb_bin, 2)
    
    # Compute error
    if nb_labels == 1 :
        error = loss (logits, y_1hot)
    else :
        error = loss (logits[0], y_1hot) + my_loss (logits[1], yb_1hot, .9)
    eta = tf.placeholder (tf.float32, name="eta")
    optimizer = tf.train.AdamOptimizer (learning_rate=eta).minimize (error)
    
    # evaluate success
    prediction = tf.argmax (tf.nn.softmax (logits[0]), axis=3)
    f1_score = get_f1_score (prediction, y_bin)
    if nb_labels == 2 :
        prediction_b = tf.argmax (tf.nn.softmax (logits[1]), axis=3)
        f1_score += get_f1_score (prediction_b, yb_bin)
        f1_score /= 2
    elif nb_labels != 1 :
        raise NotImplemented ("REEEEEE")

    if nb_labels == 1 :
        return x, y, y_bin, error, eta, optimizer, prediction, f1_score
    else :
        return x, (y, yb), (y_bin, yb_bin), error, eta, optimizer, \
            (prediction, prediction_b), f1_score


def training (train_loader, learning_rates, it, max_it, batch_size, session,
              tensor_x, tensor_y, tensor_eta, tensor_err, tensor_opti) :
    """ Subroutine applying the trainning phase """
    train_loader.fill ()
    local_eta = get_learning_rate (learning_rates, it)
    avg_err = 0
    for b in range (NB_TRAIN // batch_size) :
        batches = train_loader.get_batch (batch_size)
        if train_loader.how_many_labels () == 1 :
            image_batch, label_batch = batches
            border_batch = None
            input_dict = { tensor_x   : image_batch,
                           tensor_y   : label_batch,
                           tensor_eta : local_eta }
        else :
            image_batch, label_batch, border_batch = batches
            input_dict = { tensor_x    : image_batch,
                           tensor_y[0] : label_batch,
                           tensor_y[1] : border_batch,
                           tensor_eta  : local_eta }

        err, _ = session.run ([tensor_err, tensor_opti], feed_dict=input_dict)
        avg_err *= b      # Replace this 3 lines
        avg_err += err    # by some non-stupid
        avg_err /= b + 1  # things Mathis ...

        # Dynamic display
        progress.clear_line ()
        progress.print_percent ((b + 1) / (NB_TRAIN // batch_size),
                                "iteration {}/{} : training :".format (
                                    it + 1, max_it))
    return avg_err


def testing (test_loader, batch_size, it, max_it, session, avg_err, best_f1,
             best_f1_it, tensor_f1, tensor_x, tensor_y, saver) :
    """ Subroutine applying the testing phase, i.e.: computing F1-score """
    f1 = 0
    test_loader.fill ()
    for b in range (NB_TEST // batch_size) :
        batches = test_loader.get_batch (batch_size)
        if test_loader.how_many_labels () == 1 :
            image_batch, label_batch = batches
            f1 += session.run (tensor_f1, feed_dict={ tensor_x: image_batch,
                                                      tensor_y: label_batch })
        else :
            image_batch, label_batch, border_batch = batches
            f1 += session.run (tensor_f1,
                               feed_dict={ tensor_x    : image_batch,
                                           tensor_y[0] : label_batch,
                                           tensor_y[1] : border_batch})

        # Dynamic display
        progress.clear_line ()
        progress.print_percent ((b + 1) / (NB_TEST // batch_size),
                                "iteration {}/{} : testing :".format (
                                    it + 1, max_it))
    # Final display
    f1 /= (NB_TEST // batch_size)
    progress.clear_line ()
    print ("iteration {}/{} : F1-score = {:.3f} (err: {:.3f})".format (
        it + 1, max_it, f1, avg_err))
    
    # Save best
    if f1 > best_f1 :
        best_f1 = f1
        best_f1_it = it
        saver.save (session, "./savings/wwqu")

    return best_f1, best_f1_it


def applying (session, f1, batch_loader,
              tensor_x, tensor_y, tensor_y_bin, tensor_pred, tensor_f1) :
    """ Subroutine doing a manual interactive testing of the network """
    if f1 > 0 :
        restorer = tf.train.import_meta_graph ("./savings/wwqu.meta")
        restorer.restore (session, tf.train.latest_checkpoint ("./savings"))

    keep_going = True
    while (keep_going) :
        # Little ``parser´´
        test_img_nb = input ("Enter an image number : ")
        if test_img_nb in ['x','e', 'q', "exit", "quit", '', ''] :
            break
        try :
            test_img_nb = int (test_img_nb)
        except :
            print ("Dat iz no numberz")
            continue
        if test_img_nb < 1 or test_img_nb > 20 :
            print ("between 1 and 20")
            continue

        # Get image and label
        batches = batch_loader.get_batch (1, [test_img_nb])
        if batch_loader.how_many_labels () == 1 :
            image, label = batches
        else :
            image, label, border = batches

        # Compute
        if batch_loader.how_many_labels () == 1 :
            ybin, pred, fscore = np.array (
                session.run ([tensor_y_bin, tensor_pred, tensor_f1], 
                             feed_dict={tensor_x: image, tensor_y: label}))
        else :
            ybin, ybin_b, pred, pred_b, fscore = np.array (
                session.run ([tensor_y_bin[0], tensor_y_bin[1],
                              tensor_pred[0], tensor_pred[1], tensor_f1],
                             feed_dict={tensor_x    : image,
                                        tensor_y[0] : label,
                                        tensor_y[1] : border}))

        print ("F1-score : {}".format (fscore))

        # Some boaring treatement
        img_image = image[0]
        img_label = ybin[0]
        pred = pred[0]
        img_label *= 200
        pred *= 200            
        img_label = Image.fromarray (img_label.astype (np.uint8))          
        img_pred = Image.fromarray (pred.astype (np.uint8))
        if batch_loader.how_many_labels () == 2 :
            img_border = ybin_b[0]
            pred_b = pred_b[0]
            img_border *= 200
            pred_b *= 200
            img_border = Image.fromarray (img_border.astype (np.uint8))
            img_pred_b = Image.fromarray (pred_b.astype (np.uint8))
        
        # Display images
        Image.fromarray (img_image).show ("Input image")
        img_label.resize ((416,416)).show ("Input label")
        img_pred.resize ((416,416)).show ("Predicted label")
        if batch_loader.how_many_labels () == 2 :
            img_border.resize ((416,416)).show ("Input label border")
            img_pred_b.resize ((416,416)).show ("Predicted label border")


def main (args) :
    # Set TensorFlow's verbosity
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = str (args.tf_verbose)
    
    # Some usefull constants
    batch_size = args.batch_size
    borders    = args.borders
    nb_labels  = 2 if borders == "both" else 1
    topology   = get_topology (args.topo)
    pooling, pool_args = arg_lexer.get_pooling (args.pool[0], args.pool[1:])

    # Define the experiment
    tmp_tuple = experiement_setting (nb_labels, topology,
                                     pooling, args.pool_size, pool_args)
    x, y, y_bin, error, eta, optimizer, prediction, f1_score = tmp_tuple

    # Run the session
    init_op = tf.global_variables_initializer ()
    summary_op = tf.summary.merge_all ()
    conf = tf.ConfigProto ()
    conf.gpu_options.allow_growth = True
    with tf.Session (config=conf) as session :
        session.run (init_op)
        saver = tf.train.Saver (max_to_keep=1)

        # Prepare some dataset loaders
        train_loader = Wwqu_Loader ("train", rotation=None, borders=borders)
        test_loader  = Wwqu_Loader ("test",  rotation=None, borders=borders)
        apply_loader = Wwqu_Loader ("apply", rotation=None, borders=borders)
        
        best_f1 = 0
        best_f1_it = 0
        for iter in range (args.iterations) :
            avg_err = training (  # Tranning phase
                train_loader, args.eta, iter, args.iterations,
                batch_size, session, x, y, eta, error, optimizer)
            best_f1, best_f1_it = testing (  # Testing phase
                test_loader, batch_size, iter, args.iterations, session,
                avg_err, best_f1, best_f1_it, f1_score, x, y, saver)

        if args.tf_verbose < 3 :
            print ("Params : {}".format (args))
            print ("Max f1 = {:.3f} (at it {})".format (best_f1, best_f1_it))

        if args.apply :  # Visual interactive part
            applying (session, best_f1, apply_loader,
                      x, y, y_bin, prediction, f1_score)

            
if __name__ == "__main__" :
    parser = argparse.ArgumentParser ()
    parser.add_argument ("-a", "--application", action="store_true",
                         help="Finalize with an interactive graphical user "
                         "test.", dest="apply")
    parser.add_argument ("-b", "--batch-size", default=5, type=int, help="How "
                         "many images should be loaded and fed at the same "
                         "time.", dest="batch_size", metavar="N")
    parser.add_argument ("--borders", default=False, help="Do we extract the "
                         "borders as an actual label ? Can be either False "
                         "\"border\" or \"both\". This parameter should be "
                         "removed as soon as possible, replaced by a network "
                         "chooser.", dest="borders", metavar="type")
    parser.add_argument ("-η", "--eta", "--learning-rate", default=[.0001],
                         nargs='*', help="The perceptrons' learning rate. If "
                         "you give a float value, then it will be the constant"
                         " learning rate for the whole experiment. If you "
                         "write \"linear a b\" (with a and b float) the rate "
                         "will start at b, and decrease by a at each iter.",
                         dest="eta")
    parser.add_argument ("-l", "--loss-balance", default=.5, type=float,
                         help="ecris cette doc mathis …", dest="loss_balance",
                         metavar="ratio")
    parser.add_argument ("-n", "--nb-iterations", default=12, type=int,
                         help="How many learning iterations on the dataset",
                         dest="iterations")
    parser.add_argument ("-p", "--pooling", default=["max"], nargs='*',
                         help="What kind of pooling should be applied ? Choose"
                         " between \"max\", \"avg\", and \"power [val]\" where"
                         " val is the given power (may obly work for 1, 2, and"
                         " 4.", dest="pool", metavar="P")
    parser.add_argument ("--pool-size", default=2, type=int, help="Pooling "
                         "windows will have a [1,X,X,1] shape, this won't "
                         "affect the stride", dest="pool_size", metavar="X")
    parser.add_argument ("-t", "--topology", default="4C_4T_1P", type=str,
                         help="The topology of the network to use, where `a` "
                         "is the number of BigCONV extracting features, `b` "
                         "the number of BigCONV that will directly feed some "
                         "transconvolutionnal layers, and `c` the number of "
                         "postconv after each transconv (ie: the number of "
                         "very local conv to produce the labels)", dest="topo",
                         metavar="xC_yT_zP")
    parser.add_argument ("--verbose", default=1, type=int, help="Set "
                         "tensorflow's verbosity (3 will only show fatal "
                         "errors, 2 will add errors, 1 will add warnings, 0 "
                         "will add infos", dest="tf_verbose", metavar="V")
    main (parser.parse_args())
