"""
Everything but `create_network` is obsolete here, and will soon be removed.
"""
import tensorflow as tf
import layers

VERSION_MAJOR = 0
VERSION_MINOR = 3
VERSION_MICRO = 0
VERSION_LABEL = "power_8"

def create_network (
        in_data, nb_labels=1, nb_conv=4, nb_transconv="same", nb_postconv=1,
        double_transconv=True,
        conv_shapes=5, conv_features="auto", postconv_shapes=1,
        pool_shapes="auto", pool_strides=2, pool_fn=tf.nn.max_pool,
        relu_fn=tf.nn.leaky_relu, pool_args=None, relu_args=None) :
    """
    This is supposed to be a big FCN creator. Hightly parametrizable.

    Parameters
    ----------
    in_data : 4-Tensor
        The tensor that will serve as input for the network.'
    nb_labels : int, optional (1)
        How many different label-images should be produced byt the network.
        Usually 1, sometimes 2 when willing to also learn borders.
    nb_conv : int, optional (4)
        How many convolutional layers will be present to extract features in
        this network.
    nb_transconv : int, optional ("same")
        How many convolutional layers will be directly given to a transconv 
        one ? This value cannot be greater than `nb_conv`. If given "same", the
        value will be the same as `nb_conv`.
    nb_postconv : int, optional (1)
        How many local convolutions should be applied after the transconvs.
    double_transconv : bool, optional (True)
        Should we use double transconvs rather that simple transconv ?
    conv_shapes : int OR list<int>, optional (5)
        * If is an int : the height and width of all convolutional filters.
        * If is a list of integers : the height and width of each of the conv
          layers, from the first to the last one.
    conv_features : list<int>, optional ("auto")
        A list if integers representing the number of features present in
        successive convolutional layers, followed by thoose for the transconv.
    postconv_shapes : int OR list<int>, optional (1)
        The height and width of the post convs.
    pool_shapes : list<int> OR list<list<int>>, optional ("auto")
        A list of shapes representing the height and width of the pooling
        layers' window.
    pool_strides : int OR list<int>, optional (2)
        A list of integers representing the height and width of the pooling
        strides. if only one int is given, it then will be the shape of every
        pooling layer.
    pool_fn : function, optional (tf.nn.max_pool)
        What pooling function shoudl be applyed. I recommand one of :
        [tf.nn.max_pool, tf.nn.avg_pool, layers.p_pooling]
    relu_fn : function, optinal (tf.nn.leaky_relu)
        What activation function should be applied.
    pool_args : optional (None)
        If `pool_fn` requires arguments, put them here, not sure that it will
        be taken in account, be you can try.
    relu_args : optional (None)
        If `relu_fn` requires arguments, put them here, not sure that it will
        be taken in account, be you can try.

    Returns
    -------
    A Tensor, result of the computation of a FCN.

    Notes
    -----
    This may not be fully stable, due to the complexity of transconv layers.
    Hence I recommand, when using "auto" for at least one of `pool_shapes` or
    `pool_strides`, to also use it for the other one, and of you have faith: to
    pray. (And then to stop doing that, ditching your stupid religion, 
    realizing that as a grown up scientist, you should be smarter than that.)
    """
    # Checking and simple settings
    if nb_transconv == "same" :
        nb_transconv = nb_conv
    elif nb_transconv > nb_conv :
        raise ValueError ("There can not be more transconv than conv.")

    if conv_features == "auto" :
        conv_f  = [64 * 2 ** i for i in range (nb_conv)]
        trans_f = [conv_f[nb_conv - nb_transconv + i]           \
                   / 2 ** min (nb_conv - nb_transconv + i, 4)   \
                   for i in range (nb_transconv)]
        post_f  = [2 * 8 ** i for i in range (nb_postconv - 1, -1, -1)]
        conv_features = conv_f + trans_f + post_f
    elif type (conv_features) is not list :
        raise ValueError (
            "conv_features should either be \"auto\" or a list<int>")
    if type (conv_features) is list \
       and len (conv_features) != nb_conv + nb_transconv + nb_postconv :
        raise ValueError ("conv_features should have a length of nb_conv + "
                          "nb_transconv + nb_postconv")

    if type (conv_shapes) not in [int, list] :
        raise ValueError ("conv_shapes should either be a int or a list<int>")
    if type (conv_shapes) is list and len (conv_shapes) != nb_conv :
        raise ValueError ("conv_shapes should have a length of nb_conv")

    t = type (pool_shapes) 
    if (t not in [str, int, list]) \
       or (t is list and not type (t[0]) in [int, list]) :
        raise ValueError ("pool_shapes should be either \"auto\", a list of 4 "
                          "ints, or a list of lists of 4 ints")

    if type (pool_strides) not in [int, list] :
        raise ValueError ("pool_strides should be either an in or list of it")
    if type (pool_strides) is list and len (pool_strides) != nb_conv :
        raise ValueError ("pool_strides shoudl same length as nb_conv")
        
    
    # Create a tensorboard name
    name = ""
    if double_transconv :
        name += 'd_'
    name += str (nb_conv) + 'C_'
    name += str (nb_transconv) + 'T'

    with tf.name_scope (name) :

        # Down convolutions
        convs = []
        for i in range (nb_conv) :
            in_dt = in_data if i == 0 else convs[i - 1]

            if type (conv_features) is int :
                nb_features = conv_features
            else :
                nb_features = conv_features[i]

            if type (conv_shapes) is int :
                conv_shape = [conv_shapes, conv_shapes]
            else :
                conv_shape = [conv_shapes[i], conv_shapes[i]]

            if i < 4 :  # do some pooling, we are still at the begining
                if pool_shapes == "auto" :
                    pool_shape = [1,2,2,1]
                elif type (pool_shapes) is int :
                    pool_shape = [1, pool_shapes, pool_shapes, 1]
                else :
                    pool_shape = pool_shapes[i]

                if type (pool_strides) is int :
                    pool_stride = [1, pool_strides, pool_strides, 1]
                else :
                    pool_stride = [1, pool_strides[i], pool_strides[i], 1]

                convs.append (layers.conv_relu_pool_layer2 (
                    in_dt, nb_features, conv_shape, [1,1,1,1],
                    pool_shape, pool_stride, pool_fn,
                    relu_fn, pooling_arg=pool_args, relu_arg=relu_args))

            else :  # Size is low enough : no pooling
                convs.append (layers.conv_relu_layer (
                    in_dt, nb_features, conv_shape, relu_fn,
                    relu_args=relu_args))

        # Up transposed convolutions
        outputs = []
        for i in range (nb_labels) :
            # Actual transconvs
            trans = []
            j = 0
            for j in range (nb_transconv) :
                rank = j + (nb_conv - nb_transconv)
                up_needed = min (4, rank + 1)

                if type (conv_features) is int :
                    nb_features = conv_features
                else :
                    nb_features = conv_features[nb_conv + j]
                nb_features = int (nb_features)

                k = 0
                data = convs[rank]
                while k < up_needed :
                    if double_transconv and up_needed - k > 1 :
                        data = layers.double_transconv_type_1 (
                            data, nb_features, relu=relu_fn,
                            relu_args=relu_args)
                        k += 2

                    else :
                        data = layers.transconv_layer (
                            data, nb_features, [2,2], [1,2,2,1])
                        k += 1
                trans.append (data)

            # Feature merging
            concat = tf.concat (trans, 3)
            for j in range (nb_postconv) :
                if type (conv_features) is int :
                    nb_features = conv_features
                else :
                    nb_features = conv_features[nb_conv + nb_transconv + j]

                if type (postconv_shapes) is int :
                    postconv_shape = [postconv_shapes, postconv_shapes]
                else :
                    postconv_shape = [postconv_shapes[j], postconv_shapes[j]]

                concat = layers.conv_relu_layer (
                    concat, nb_features, postconv_shape)

            outputs.append (concat)
        return outputs
    
