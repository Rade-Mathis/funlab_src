import tensorflow as tf
import layers

def get_pooling (name :str, args=None) :
    if name in ["max", "maximum", "max_pool", "max_pooling"] :
        return tf.nn.max_pool, None
    elif name in ["avg", "average", "mean", "avg_pool", "avg_pooling"] :
        return tf.nn.avg_pool, None
    elif name in ["pow", "power", "pow_pool", "power_pool", "pow_pooling",
                  "power_pooling"] :
        try :
            arg = None if args == [] or args[0] is None else float (args[0])
            return layers.p_pooling, arg
        except :
            raise ValueError ("power pooling requires either None or floating "
                              "argument, not {}".format (args[0]))

    else :
        raise ValueError ("{} is not a recognized pooling name".format (name))
