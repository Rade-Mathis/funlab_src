# TODO :

# un reseau de la forme :
## un enchainement de (quatre) CONV-POOL-ReLU classiques
## un CONV-SPP(-ReLU)
## (deux) FC

# un affichage des images, p-e

"""
This file tries to build a CNN with a SPP layer in order to train recognizing
the caracter '7' in a picture.
"""

import tensorflow as tf
from PIL import Image
import csv
import sys

import progress as util

ROOT_FOLDER = "/home/rademathis/work"
TRAIN_LABELS_FILE = ROOT_FOLDER \
                    + "/datasets/housenumbers_full/train/digitStruct_7.csv"
TEST_LABELS_FILE  = ROOT_FOLDER \
                    + "/datasets/housenumbers_full/test/digitStruct_7.csv"

verbose = True

def open_png (filename) :
    """ 
    Open a PNG file as an image tensor - may not be an optimal function
    """
    img = Image.open (filename)
    width, heigh = img.size
    return [[ img.getpixel ((x, y)) \
              for y in range (heigh)] for x in range (width)]
    
def open_train_png (img_number) :
    return open_png (
        ROOT_FOLDER + "/datasets/housenumbers_full/train/{}.png".format (
            img_number))

def open_test_png (img_number) :
    return open_png (
        ROOT_FOLDER + "/datasets/housenumbers_full/test/{}.png".format (
            img_number))

def conv_relu_layer (in_data, nb_filters, filter_shape, name=None) :
    """ 
    Create a convolutional layer, followed by a non-linear activation one.

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    name : string, optional
        name of the return tensorflow's object

    Returns
    -------
    A new tf CONV layer
    """
    # setups
    w_name = None if name is None else name + "_W"
    b_name = None if name is None else name + "_b"
    f_name = None if name is None else name + "_f"
    if len (in_data.shape) == 3 :
        in_data_reshaped = tf.expand_dims (in_data, 0)
    else :
        in_data_reshaped = tf.identity (in_data)
    nb_in_channels = int (in_data_reshaped.shape[3])
    conv_shape = [filter_shape[0], filter_shape[1], nb_in_channels, nb_filters]

    # Trainable tensors
    weights = tf.Variable (
        tf.truncated_normal (conv_shape, mean=0., stddev=.05),  # todo: tune it
        name=w_name)
    bias = tf.Variable (
        tf.truncated_normal ([nb_filters], mean=0., stddev=1.),  # todo: idem
        name=b_name)

    # Compute
    output = tf.nn.conv2d (in_data_reshaped, weights,
                           [1,1,1,1], padding="SAME")
    output += bias
    output = tf.nn.relu (output, name=f_name)
    return output


def conv_relu_pool_layer (in_data, nb_filters, filter_shape, pool_shape,
                          name=None) :
    """ 
    Create a typical 'full CONV layer': CONV-ReLU-POOL

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    pool_shape : list
        the classical (1 and 2) dimension of the pooling window
    name : string, optional
        name of the returned tensorflow's object

    Returns
    -------
    A tf opreation performing a CONV-ReLU-maxpool operation layer.
    """
    c_name = "conv" if name is None else name + "_conv"
    p_name = "pool" if name is None else name + "_pool"
    conv_out = conv_relu_layer (in_data, nb_filters, filter_shape, c_name)
    ksize = [1, pool_shape[0], pool_shape[1], 1]  # question it again
    strides = [1, 2, 2, 1]  # question it also
    return tf.nn.max_pool (conv_out, ksize=ksize, strides=strides,
                           padding="SAME", name=p_name)


def spp_layer (in_data, levels=[7,3,1], name=None) :  # arbitrary default level
    """
    Create a Spatial Pyramid Pooling layer.

    Parameters
    ----------
    in_data : 4-tensor (batch, heigh, width, channels)
        The input data, ie: X, or the output of the previous layer
    levels : list, optionnal
        The diffrent levels of the pyramid, ie: the number of the diffrent
        pooling that will be applied in //
        By default `levels` is `[7, 3, 1]`
    name : string, optional
        name of the returned tensorflow object

    Returns
    -------
    A tensorflow object performing a SPP operation layer.

    Notes
    -----
    Imported from :
        - RikHeijdens@https://github.com/tensorflow/tensorflow/issues/6011
        - https://github.com/luizgh/Lasagne/commit/c01e3d922a5712ca4c54617a15a794c23746ac8c
        - https://arxiv.org/pdf/1406.4729.pdf
    """
    in_shape = tf.shape (in_data)
    in_heigh = tf.cast (tf.gather (in_shape, 1), tf.int32)
    in_width = tf.cast (tf.gather (in_shape, 2), tf.int32)
    
    spp_out = []
    for lvl in levels :
        lvl_out = []
        for row in range (lvl) :
            for col in range (lvl) :
                # floor ((row / lvl) * in_heigh)
                start_h = tf.cast (
                    tf.floor (tf.multiply (tf.divide (row, lvl),
                                           tf.cast (in_heigh, tf.float32))),
                    tf.int32)
                # ceil (((row + 1) / lvl) * in_heigh)
                end_h = tf.cast (
                    tf.ceil (tf.multiply (tf.divide ((row + 1), lvl),
                                          tf.cast(in_heigh, tf.float32))),
                    tf.int32)
                # floor ((col / lvl) * in_width)
                start_w = tf.cast (
                    tf.floor (tf.multiply (tf.divide (col, lvl),
                                           tf.cast(in_width, tf.float32))),
                    tf.int32)
                # ceil (((col + 1) / lvl) * in_width)
                end_w = tf.cast (
                    tf.ceil (tf.multiply (tf.divide ((col + 1), lvl),
                                          tf.cast(in_width, tf.float32))),
                    tf.int32)
                
                pooling_region = in_data[:, start_h:end_h, start_w:end_w, :]
                pool_result = tf.reduce_max (pooling_region, axis=(1, 2))
                lvl_out.append (pool_result)
        
        spp_out += lvl_out
    return tf.concat (spp_out, 1, name=name)
        

def conv_relu_spp_layer (in_data, nb_filters, filter_shape, levels=[7,3,1],
                         name=None) :
    """ 
    Create a CONV-ReLU-SPP layer

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    levels : list, optionnal
        The diffrent levels of the pyramid, ie: the number of the diffrent
        pooling that will be applied in //
        By default `levels` is `[7, 3, 1]`
    name : string, optional
        name of the returned tensorflow's object

    Returns
    -------
    A tf opreation performing a CONV-ReLU-SPmaxPool operation layer.
    """
    spp_name = "spp" if name is None else name+"_spp"
    conv_out = conv_relu_layer (in_data, nb_filters, filter_shape, name)
    return spp_layer (conv_out, levels, spp_name)


def fc_layer (in_data, out_size: int, mean=0., stddev=.02, name=None) :
    """
    A tipical fully connected layer of perceptrons.

    Parameters
    ----------
    in_data : 2-Tensor (batch, nb_inputs)
        The input data (X, or previous layer's output)
    out_size : int
        How many outputed features do you want, ie: how many neurones in the 
        layer.
    mean : float, optional
        Weigth initialization.
    stddev : float, optional
        Weigth initialization.
    name : string, optional
        name of the returned TensorFlow object.

    Returns
    -------
    A TensorFlow object performing a fully connected layer operation.
    """
    w_name = None if name is None else name+"_w"
    b_name = None if name is None else name+"_b"
    nb_inputs = int (in_data.shape[1])
    
    w = tf.Variable (tf.truncated_normal ([nb_inputs, out_size],
                                          mean=mean, stddev=stddev,
                                          name=w_name))
    b = tf.Variable (tf.truncated_normal ([out_size],
                                          mean=mean, stddev=stddev,
                                          name=w_name))
    return tf.add (tf.matmul (in_data, w), b, name=name)


def calculate_metrics (tp, tn, fp, fn, sig=None) :
    """
    Given the results of a test, calculate accuracy, precision and recall.

    Parameters
    ----------
    tp: int
        True positives (label == prediction == 1)
    tn: int
        True negatives (label == prediction == 0)
    fp: int
        False positives (label == 0 ; prediction == 1)
    fn: int
        False negatives (label == 1 ; prediction == 0)
    sig: int, optional
        Will round the returned results to the closer value with `sig` decimal
        numbers. If `sig == None` the result won't be round.
        By default, `sig` worth `None`

    Returns
    -------
    Three floats: the accuracy, the precision and the recall.
    """
    full = tp + tn + fp + fn
    if full == 0 :
        acc = None
    else :
        acc = (tp + tn) / full
        if not sig is None :
            acc = round (acc, sig)

    pos = tp + fp
    if pos == 0 :
        pre = None
    else :
        pre = tp / pos
        if not sig is None :
            pre = round (pre, sig)

    one = tp + fn
    if one == 0 :
        rec = None
    else :
        rec = tp / one
        if not sig is None :
            rec = round (rec, sig)

    return acc, pre, rec
        

def main () :  # todo
    # set parameters
    eta = .0001  # .0001
    iterations = 50
    batch_size = 1  # I've no gpu and size vary, so ... dunno
    nb_train = 5000
    nb_test  = 100
    # what about momentum ?... look if it's contained inside something else ...

    # Load data
    x = tf.placeholder (tf.float32, [None, None, 3])
    y = tf.placeholder (tf.float32, [1, 2])

    # Create the network topology
    tiny_c1 = conv_relu_pool_layer (x,  8,  [3,3], [2,2], "tC1")
    big_c1  = conv_relu_pool_layer (x,  8,  [9,9], [3,3])
    tiny_c2 = conv_relu_spp_layer  (tiny_c1, 16, [3,3], [7,4,1])
    big_c2  = conv_relu_spp_layer  (big_c1,  16, [9,9], [7,4,1])
    combined = tf.concat ([tiny_c2, big_c2], 1)
    f1 = tf.nn.relu    (fc_layer (combined, 30))
    f2 = fc_layer (f1, 2)
    prediction = tf.nn.softmax (f2)

    # Define a cross-entropy function
    error = tf.reduce_mean (tf.nn.softmax_cross_entropy_with_logits (
        logits=f2,
        labels=y))

    # Create an optimizer
    # optimizer = tf.train.AdamOptimizer (learning_rate=eta).minimize (error)
    optimizer = tf.train.GradientDescentOptimizer (
        learning_rate=eta).minimize (error)

    # Create an accuracy calculator
    pred_is_ok = tf.equal (tf.argmax (y, 1), tf.argmax (prediction, 1))
    accuracy = tf.reduce_mean (tf.cast (pred_is_ok, tf.float32))  # delthis
    
    # Run the session
    init_op = tf.global_variables_initializer ()
    with tf.Session () as session :
        session.run (init_op)
        nb_train_img = None
        nb_test_img  = None

        # Launch one run
        for i in range (iterations + 1) :
            avg_err = 0
            # Train
            with open (TRAIN_LABELS_FILE, "r") as csv_file :
                reader = csv.reader (csv_file, delimiter=',')
                line_n = 0
                for j in reader :
                    # reduce time by reducing dataset
                    if line_n >= nb_train :
                        break

                    # skipping header OR get size only
                    line_n += 1
                    if line_n == 1 or i == 0 :
                        continue
                    
                    # Feed one case
                    image = open_train_png (int (j[0]))
                    label = [[int (j[1] == "True"),
                              int (j[1] == "False")]]
                    _, err = session.run ([optimizer, error],
                                          feed_dict={x: image, y:label})
                    avg_err += err / nb_train_img

                    # debug :
                    # var = tf.get_variable ("C1_conv_W")
                    # print (session.run (var, feed_dict={x: image}).shape)

                    if verbose and line_n % 10 == 2:
                        util.clear_line ()
                        util.print_percent (
                            line_n / nb_train_img,
                            "Iteration nb {} training:".format (i))

            # Test
            # avg_accuracy = 0
            true_positives  = 0
            true_negatives  = 0
            false_positives = 0
            false_negatives = 0
            with open (TEST_LABELS_FILE, "r") as csv_file :
                reader = csv.reader (csv_file, delimiter=',')
                line_m = 0
                for j in reader :
                    # reduce time by reducing dataset
                    if line_m >= nb_test :
                        break

                    # skipping header OR get size only
                    line_m += 1
                    if line_m == 1 or i == 0 :
                        continue

                    # test one image
                    image = open_test_png (int (j[0]))
                    label = [[int (j[1] == "True"),
                              int (j[1] == "False")]]
                    correct = session.run (pred_is_ok,
                                           feed_dict={x: image, y: label})
                    true_positives  += label[0][0] * int (correct)
                    true_negatives  += label[0][1] * int (correct)
                    false_positives += label[0][1] * int (not correct)
                    false_negatives += label[0][0] * int (not correct)

                    # avg_accuracy += int (correct) / nb_test_img

                    if verbose and line_m % 10 == 2:
                        util.clear_line ()
                        util.print_percent (
                            line_m / nb_test_img,
                            "Iteration nb {} testing:".format (i))

            if i == 0 :
                nb_train_img = line_n - 1  # -1 is to forget the header
                nb_test_img  = line_m - 1  # idem
            else :
                accuracy, precision, recall = calculate_metrics (
                    true_positives, true_negatives,
                    false_positives, false_negatives, 3)
                util.clear_line ()
                print (("Iteration nb {} : err = {:.3f}\t acc = {}\t "
                        + "pre = {}\t rec = {}")
                       .format (i, avg_err, accuracy, precision, recall))

            
if __name__ == "__main__" :
    print ("TODOs are written in the source code")
    print ("###")
    print ("### STARTING - {}".format (sys.argv[0]))
    print ("###")
    main ()
    
