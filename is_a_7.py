import pandas as pd
import csv
import sys

def main (filename) :
    in_data = pd.read_csv (filename)
    has_7 = pd.DataFrame (columns=["name", "has_7"])
    size = len (in_data)
    i = 0  # dynadisp
    for line in range (size) :
        i += 1  # dynadisp
        name = int (in_data["FileName"][line].split ('.')[0])
        labl = in_data["DigitLabel"][line]
        if labl == 7 :
            has_7.loc[name] = [name, True]
        else :
            if not name in has_7.index :
                has_7.loc[name] = [name, False]

        # dynadisp
        if i % 1000 == 0 :
            print (end="\rreading: {}%".format (i * 100 // size))

    print ("\rAll {} has been read.")
    print ("Writting new file")

    has_7.to_csv (filename.split('.')[0]+"_7.csv", sep=',', index=False)

if __name__ == "__main__" :
    if len (sys.argv) < 2 :
        print ("usage : {} filename.csv".format (sys.argv[0]))
        exit (1)
    main (sys.argv[1])
