from PIL import Image
import numpy as np
import os
import sys

def to_borders (image :np.ndarray, radius=1) :
    out = np.zeros (image.shape)
    x = image.shape[0]
    y = image.shape[1]
    for i in range (x) :
        for j in range (y) :
            is_border = False
            for ii in range (max (0,     i - radius),
                             min (x - 1, i + radius) + 1) :
                for jj in range (max (0,     j - radius),
                                 min (y - 1, j + radius) + 1) :
                    if image[ii,jj] != image[i,j] :
                        is_border = True
                        break  # We only break one, so we MAY waste time. But I
                               # think this may still be better that the time
                               # consumed by an additional IF
                    
            if is_border :
                out[i,j] = 1
                
    return out


def extract_all (folder_localization, radius=1) :
    filenames = [i for i in os.listdir (folder_localization) if "anno" in i]
    for i in range (len (filenames)) :
        print (end="\r{} / {}".format (i, len (filenames)))
        sys.stdout.flush ()
        filename = folder_localization + '/' + filenames[i]
        image = np.array (Image.open (filename))
        borders = to_borders (image, radius)
        file_id = filename.split ('_')
        newname = ""
        for j in range (len (file_id) - 1) :
            newname += file_id[j] + '_'
        newname += "border.bmp"
        Image.fromarray (borders.astype (np.uint8)).save (newname)
    print()
        

if __name__ == "__main__" :
    if len (sys.argv) == 2 :
        extract_all (sys.argv[1])
    elif len (sys.argv) == 3 :
        extract_all (sys.argv[1], int (sys.argv[2]))

    elif len (sys.argv) == 42 :
        print ("""
Vous savez, moi je ne crois pas qu’il y ait de bonne ou de mauvaise situation.
Moi, si je devais résumer ma vie aujourd’hui avec vous, je dirais que c’est
d’abord des rencontres. Des gens qui m’ont tendu la main, peut-être à un 
moment où je ne pouvais pas, où j’étais seul chez moi. Et c’est assez curieux 
de se dire que les hasards, les rencontres forgent une destinée... Parce que 
quand on a le goût de la chose, quand on a le goût de la chose bien faite, le 
beau geste, parfois on ne trouve pas l’interlocuteur en face je dirais, le 
miroir qui vous aide à avancer. Alors ça n’est pas mon cas, comme je disais là,
puisque moi au contraire, j’ai pu : et je dis merci à la vie, je lui dis merci,
je chante la vie, je danse la vie... je ne suis qu’amour ! Et finalement, quand
beaucoup de gens aujourd’hui me disent « Mais comment fais-tu pour avoir cette 
humanité ? », et bien je leur réponds très simplement, je leur dis que c’est ce
goût de l’amour ce goût donc qui m’a poussé aujourd’hui à entreprendre une 
construction mécanique, mais demain qui sait ? Peut-être simplement à me mettre
au service de la communauté, à faire le don, le don de soi...
        """)

    else :
        print ("usage : {} <folder name> [radius]        (default radius is 1)"
               .format (sys.argv[0]))
