import tensorflow as tf

# File constants
VERSION_MAJOR = 0
VERSION_MINOR = 5
VERSION_MICRO = 0
VERSION_LABEL = "dynamic_batch_transconv"


def lrelu (x, alpha=.01) :
    """ Leaky relu  -- tf.nn also does that, this function is obsolete """
    import sys
    print (
        """Function ``layers.lrelu'' is obsolete and will be removed in version
        1.0.0, you should use ``tf.nn.leaky_relu'' instead.""",
        file=sys.stderr)
    return tf.nn.relu(x) - alpha * tf.nn.relu(-x)


def p_pooling (in_data, ksize, strides, padding, power=2, name=None) :
    """
    Perform a power-pooling, i.e. a kind of average pooling where all elements 
    are first elevated to a power, and the mean is rooted by this power, givng
    results situated between avg-pooling and max-pooling.

    Parameters
    ----------
    in_data : 4-D Tensor
        The data to pool
    ksize : 1-D int Tensor of 4 elements
        The size of the window
    strides : 1-D int Tensor of 4 elements
        The stride of the sliding window
    padding : string
        either "VALID" or "SAME"
    power : float, optional
        The power at which the values will be elevated.
        Note that power can also be +infinity
    name : string, optional
        Used to give a name to the TensorFlow oprators

    Returns
    -------
    A 4-D Tensor, its shape depending on in_data's one and padding.

    Notes
    -----
    * If `power == 1' then p_pooling is acting like an average pooling.
      If `power == +inf' then p_pooling is acting like a max pooling.
      power values such as NaN, -inf and 0 are actualy bad ideas.

    * This layers is inspired by Lp-pooling one. (cf: P.Sermanet et al. 
      "Convolutional Neural Networks Applied to House Numbers Digit
      Classification"), but isn't working the same way since I don't understand
      how Lp-pooling are actually implemented.
    """
    with tf.name_scope ("Ppool") :
        if power == float ("+inf") :
            return tf.nn.max_pool (in_data, ksize, strides, padding, name=name)
        powered = in_data ** power
        average_pooled = tf.nn.avg_pool (powered, ksize, strides, padding,
                                         name=name)
        return average_pooled ** (1. / power)


# def mixed_pooling (in_data, ksize, strides, padding,
#                    pool_a=tf.nn.max_pool, pool_b=tf.nn.avg_pool,
#                    a_args=None, b_args=None) :
#     """ prototype """
#     with tf.name_scope (mixed_pool) :
#         # Get the two poolings
#         if a_args is None :
#             a_out = pool_a (in_data, ksize, strides, padding)
#         else :
#             a_out = pool_a (in_data, ksize, strides, padding, a_args)
#         if b_args is None :
#             b_out = pool_b (in_data, ksize, strides, padding)
#         else :
#             b_out = pool_b (in_data, ksize, strides, padding, b_args)

#         # Combine them
#         weight = tf.Variable (tf.truncated_normal ([1], mean=.5, stddev=.01))
#         return (weight * a_out)  + ((1 - weight) * b_out)


def conv_relu_layer (in_data, nb_filters, filter_shape, relu=tf.nn.leaky_relu,
                     name=None, stride=[1,1,1,1], relu_args=None,
                     padding="SAME"):
    """ 
    Create a convolutional layer, followed by a non-linear activation one.

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    name : string, optional
        name of the return tensorflow's object
    stride : 4-list<int>
        How much do we move the conv window [batch, height, width, channels]
    
    Returns
    -------
    A new tf CONV layer
    """
    with tf.name_scope ("CONV-ReLU") :
        # setups
        w_name = None if name is None else name + "_W"
        b_name = None if name is None else name + "_b"
        f_name = None if name is None else name + "_f"
        if len (in_data.shape) == 3 :
            in_data_reshaped = tf.expand_dims (in_data, 0)
        else :
            in_data_reshaped = tf.identity (in_data)
        nb_in_channels = int (in_data_reshaped.shape[3])
        conv_shape = [filter_shape[0], filter_shape[1],
                      nb_in_channels, nb_filters]

        # Trainable tensors
        weights = tf.Variable (  # todo : tune the mean and stddev
            tf.truncated_normal (conv_shape, mean=0., stddev=.01),
            name=w_name)
      
        bias = tf.Variable (  # todo : idem
            tf.truncated_normal ([nb_filters], mean=0., stddev=.01),
            name=b_name)

        # Compute
        output = tf.nn.conv2d (in_data_reshaped, weights, stride,
                               padding=padding)
        output += bias

        if relu is not None :
            if relu_args is None :
                return relu (output)
            else :
                return relu (output, relu_args)
        return output


def conv_relu_pool_layer (in_data, nb_filters, filter_shape, pool_shape,
                          pooling=tf.nn.max_pool, relu=tf.nn.leaky_relu,
                          name=None, arg=None) :
    """ 
    Create a typical 'full CONV layer': CONV-ReLU-POOL
    This version is here for backcompatibility, it just calls 
    conv_relu_pool_layer2 to keep the old prototype

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    pool_shape : list
        the classical (1 and 2) dimension of the pooling window
    pooling : function
        the kind of applyed pooling
        By default, we apply a max-pooling
    name : string, optional
        name of the returned tensorflow's object
    arg : aditional argument
        May be requiered for some specific pooling function, eg:
        - p_pooling requires a float to specify the power

    Returns
    -------
    A tf opreation performing a CONV-ReLU-maxpool operation layer.
    """
    with tf.name_scope ("CONV-ReLU-POOL") :
        return conv_relu_pool_layer2 (
            in_data, nb_filters, filter_shape, [1,1,1,1],
            [1, pool_shape[0], pool_shape[1], 1],
            [1, pool_shape[0], pool_shape[1], 1], pooling, relu, name, arg)

def conv_relu_pool_layer2 (in_data,
                           nb_filters, filter_shape, filter_stride=[1,1,1,1],
                           pool_shape=[1,2,2,1], pool_stride=[1,2,2,1],
                           pooling=tf.nn.max_pool, relu=tf.nn.leaky_relu,
                           name=None, pooling_arg=None, relu_arg=None) :
    """ 
    Create a typical 'full CONV layer': CONV-ReLU-POOL

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in parallel
    filter_shape : 2-list<int>
        heigh and width of the filters
    filter_stride : 4-list<int>
        How much we will move the filter to reaply it [batch, height, width, 
        channels] (note : batch and channels should be 1)
    pool_shape : 4-list<int>
        the size of the window, commonly [1,x,x,1]
    pool_stride : 4-list<int>
        How much will we move the pooling window each time. If pool_stride ==
        pool_shape, then it will be a non overlapping situation.
    pooling : function
        the kind of applyed pooling
        By default, we apply a max-pooling
    rely : function
        The kind of applied activation function
        By default, we apply a leaky-ReLU
    name : string, optional
        name of the returned tensorflow's object
    pooling_arg : additional argument
        May be requiered for some specific pooling function, eg:
        - p_pooling requires a float to specify the power
    relu_arg : additional argument
        May be requiered for some specific activation function, eg:
        - tf.nn.leaky_relu requires a float to specify the leak

    Returns
    -------
    A tf opreation performing a CONV-ReLU-maxpool operation layer.
    """
    with tf.name_scope ("CONV-ReLU-POOL") :
        c_name = "conv" if name is None else name + "_conv"
        p_name = "pool" if name is None else name + "_pool"
        conv_out = conv_relu_layer (in_data, nb_filters, filter_shape, relu,
                                    c_name, filter_stride, relu_arg)
        ksize = pool_shape
        strides = pool_stride
        if pooling in [tf.nn.max_pool, tf.nn.avg_pool] :
            return pooling (conv_out, ksize=ksize, strides=strides,
                            padding="SAME", name=p_name)
        elif pooling is p_pooling :
            if pooling_arg is None :
                raise ValueError ("{} need an additional argument"
                                  .format (pooling))
            return pooling (conv_out, ksize, strides, "SAME", pooling_arg,
                            p_name)


def fc_layer (in_data, out_size: int, mean=0., stddev=.02, name=None) :
    """
    A tipical fully connected layer of perceptrons.

    Parameters
    ----------
    in_data : 2-Tensor (batch, nb_inputs)
        The input data (X, or previous layer's output)
    out_size : int
        How many outputed features do you want, ie: how many neurones in the 
        layer.
    mean : float, optional
        Weigth initialization.
    stddev : float, optional
        Weigth initialization.
    name : string, optional
        name of the returned TensorFlow object.

    Returns
    -------
    A TensorFlow object performing a fully connected layer operation.
    """
    with tf.name_scope ("FC") :
        w_name = None if name is None else name+"_w"
        b_name = None if name is None else name+"_b"
        nb_inputs = int (in_data.shape[1])
    
        w = tf.Variable (tf.truncated_normal ([nb_inputs, out_size],
                                              mean=mean, stddev=stddev,
                                              name=w_name))
        b = tf.Variable (tf.truncated_normal ([out_size],
                                              mean=mean, stddev=stddev,
                                              name=w_name))
        return tf.add (tf.matmul (in_data, w), b, name=name)


def transconv_layer (in_data, nb_filters, filter_shape, stride,
                     padding="VALID", relu=tf.nn.leaky_relu, relu_args=None) :
    """ 
    Create a transposed convolutionnal layer (a.k.a.: deconvolutionnal layer)

    Parameters
    ----------
    in_data : 4-Tensor
        The data to transconvolve
    nb_filters : int
        The number of convolution computed in parallel (ie: the number of 
        features in the ouputed tensor.
    filter_shape : 2-list<int>
        The dimention of the convolution, ie: the number of output pixels
        influenced by one input pixel.
    stride : 4-list<int>
        Should be [1,x,y,1], since TensorFlow doesn't implement wider way to do
        so right now. "How much we move in the output while moving by one in 
        the input." Or more likely : how much will we explode the input matrix
        before transconving on it.
    padding : str, optional (default: "SAME")
        Can be either "SAME" or "VALID"
        - "SAME" won't padd anything
        - "VALID" will padd with zeros so each input pixel is considered the
          same number of time.
    relu : function (in_data :tf.Tensor [, arg]), optional (default:
           tf.nn.leaky_relu)
        The activation function to use after the actual transposed convolution.
    relu_args : optinal (default: None)
        If the activation requires an argument, give it here, None will be
        ignored.

    Returns
    -------
    A 4-TensorFlow.Tensor ready to compute a transconv+activation layer.

    Note
    ----
    This may help you : github.com/vdumoulin/conv_arithmetic
    What they call "no padding" is our "VALID", and "full padding" is our 
    "SAME".
    """
    with tf.name_scope ("transconv") :
        # Get input dimensions
        batch_size  = tf.shape (in_data)[0]
        in_height   = int (in_data.shape[1])
        in_width    = int (in_data.shape[2])
        in_channels = int (in_data.shape[3])

        # Calculate output dimensions
        if padding == "SAME" :
            out_height = (in_height - 1) * stride[1] + 1
            out_width  = (in_width  - 1) * stride[2] + 1
        elif padding == "VALID" :
            out_height = (in_height - 1) * stride[1] + filter_shape[0]
            out_width  = (in_width  - 1) * stride[2] + filter_shape[1]
        else :
            raise ValueError ("Unknown padding type : {}".format (padding))
        out_shape = tf.stack ([batch_size, out_height, out_width, nb_filters])

        # Creating the variables
        conv_shape = [filter_shape[0], filter_shape[1],
                      nb_filters, in_channels]
        weight = tf.Variable (tf.truncated_normal (conv_shape,   0., .01))
        bias   = tf.Variable (tf.truncated_normal ([nb_filters], 0., .01))

        # Making the convolution
        output = tf.nn.conv2d_transpose (in_data, weight,
                                         out_shape, stride, padding)
        output = tf.nn.bias_add (output, bias)

        # Applying the activation function
        if relu is None :
            return output
        elif relu_args is None :
            return relu (output)
        else :
            return relu (output, relu_args)


def double_transconv_type_1 (in_data, nb_filters,
                             relu=tf.nn.leaky_relu, relu_args=None) :
    """
    Here, rather than building a simple transposed_convolution, we simply build
    two of them, following each other in the way that it produces an output of 
    size 4 times bigger than the output, using at one moment a filter size
    wider than the stride, permitting some overlapping I hope to be usefull.

    Paramters
    ---------
    in_data : 4-Tensor
        The data to transconvolve
    nb_filters : int
        The number of convolution computed in parallel (ie: the number of 
        features in the ouputed tensor. Common to the 2 transconvolutions.
    relu : function (in_data :tf.Tensor [, arg]), optional (default:
           tf.nn.leaky_relu)
        The activation function to apply after each transposed convolution.
    relu_args : optinal (default: None)
        If the activation requires an argument, give it here, None will be
        ignored.
    """
    ## TODO ## Try to change the first filter shape ##
    t1 = transconv_layer (in_data, nb_filters, [2,2], [1,2,2,1], "SAME")
    t2 = transconv_layer (t1,      nb_filters, [4,4], [1,2,2,1], "VALID")
    return t2
