# Computation imports
import math
import tensorflow as tf

# Personal imports
import layers  # Implementing neural layers based on TensorFlow

# File constants
VERSION_MAJOR = 0
VERSION_MINOR = 0
VERSION_MICRO = 0
VERSION_LABEL = "init"


def get_network (codename :str) :
    """ 
    Returns a function creating a network
    Function prototype will be :
    functionname (in_data, image_size=150, nb_class=8, name=???)
    """
    if codename == "2x1_2" :
        return _network_2C_2F
    elif codename == "3x1_2" :
        return _network_3C_2F
    elif codename == "4x1_2" :
        return _network_4C_2F
    elif codename == "4x2_2" :
        return _network_2x4C_2F
    else :
        raise ValueError ("[KATHER_NETWORKS] {} is not a know network name"
                          .format (codename))
    

def _network_2C_2F (in_data, image_size=150, nb_class=8, name="net4C2F") :
    """
    Create a network composed of 2 [CONV - leaky ReLU - maxPOOL] layers,
    followed by 2 fully connected layers.

    Parameters
    ----------
    in_data : 4-tensor (shape = batch, height, width, colors)
        The input data to compute
    image_size : int
        The height of the image in pixels (images has to be squared)
    nb_class : int
        The number of class to identify
    name : string, optional
        Used to name TensorFlow graph nodes

    Returns
    -------
    A TensorFlow 4-tensor of the predicated classes
    """
    c2_size = math.ceil (image_size / 2**3)
    crpl = layers.conv_relu_pool_layer  # alias
    c1 = crpl (in_data, 32, [5,5], [4,4], name=name+"_C1")
    c2 = crpl (c1,     128, [5,5], [2,2], name=name+"_C2")
    c2_flattened = tf.reshape (c2, [-1, 128 * c2_size * c2_size])
    fc1 = layers.lrelu (layers.fc_layer (c2_flattened, 64, name=name+"_FC1"))
    fc2 = layers.fc_layer (fc1, nb_class, name=name+"_FC2")
    return fc2


def _network_3C_2F (in_data, image_size=150, nb_class=8, name="net4C2F") :
    """
    Create a network composed of 3 [CONV - leaky ReLU - maxPOOL] layers,
    followed by 2 fully connected layers.

    Parameters
    ----------
    in_data : 4-tensor (shape = batch, height, width, colors)
        The input data to compute
    image_size : int
        The height of the image in pixels (images has to be squared)
    nb_class : int
        The number of class to identify
    name : string, optional
        Used to name TensorFlow graph nodes

    Returns
    -------
    A TensorFlow 4-tensor of the predicated classes
    """
    c3_size = math.ceil (image_size / 2**3)
    crpl = layers.conv_relu_pool_layer  # alias

    c1 = crpl (in_data, 32, [5,5], [2,2], name=name+"_C1")
    c2 = crpl (c1,      64, [5,5], [2,2], name=name+"_C2")
    c3 = crpl (c2,     128, [5,5], [2,2], name=name+"_C3")
    c3_flattened = tf.reshape (c3, [-1, 128 * c3_size * c3_size])
    fc1 = layers.lrelu (layers.fc_layer (c3_flattened, 64, name=name+"_FC1"))
    fc2 = layers.fc_layer (fc1, nb_class, name=name+"_FC2")
    return fc2


def _network_4C_2F (in_data, image_size=150, nb_class=8, name="net4C2F") :
    """
    Create a network composed of 4 [CONV - leaky ReLU - maxPOOL] layers, 
    followed by 2 fully connected layers.

    Parameters
    ----------
    in_data : 4-tensor (shape = batch, height, width, colors)
        The input data to compute
    image_size : int
        The height of the image in pixels (images has to be squared)
    nb_class : int
        The number of class to identify
    name : string, optional
        Used to name TensorFlow graph nodes

    Returns
    -------
    A TensorFlow 4-tensor of the predicated classes
    """
    c4_size = math.ceil (image_size / 2**4)
    crpl = layers.conv_relu_pool_layer  # alias
    
    c1 = crpl (in_data, 32, [5,5], [2,2], name=name+"_C1")
    c2 = crpl (c1,      64, [5,5], [2,2], name=name+"_C2")
    c3 = crpl (c2,     128, [5,5], [2,2], name=name+"_C3")
    c4 = crpl (c3,     256, [5,5], [2,2], name=name+"_C4")
    c4_flattened = tf.reshape (c4, [-1, 256 * c4_size * c4_size])
    fc1 = layers.lrelu (layers.fc_layer (c4_flattened, 64, name=name+"_FC1"))
    fc2 = layers.fc_layer (fc1, nb_class, name=name+"_FC2")
    return fc2


def _network_2x4C_2F (in_data, image_size=150, nb_class=8, name="net4C2F") :
    """
    Create a network composed of 4 [(CONV - leaky ReLU) x 2 - maxPOOL] layers, 
    followed by 2 fully connected layers.

    Parameters
    ----------
    in_data : 4-tensor (shape = batch, height, width, colors)
        The input data to compute
    image_size : int
        The height of the image in pixels (images has to be squared)
    nb_class : int
        The number of class to identify
    name : string, optional
        Used to name TensorFlow graph nodes

    Returns
    -------
    A TensorFlow 4-tensor of the predicated classes
    """
    c4_size = math.ceil (image_size / 2**4)
    crl  = layers.conv_relu_layer  # alias
    crpl = layers.conv_relu_pool_layer  # alias
    
    c1_1 = crl (in_data, 32, [5,5],        name=name+"_C1_1")
    c1_2 = crpl (c1_1,   32, [5,5], [2,2], name=name+"_C1_2")
    c2_1 = crl  (c1_2,   64, [5,5],        name=name+"_C2_1")
    c2_2 = crpl (c2_1,   64, [5,5], [2,2], name=name+"_C2_2")
    c3_1 = crl  (c2_2,  128, [5,5],        name=name+"_C3_1")
    c3_2 = crpl (c3_1,  128, [5,5], [2,2], name=name+"_C3_2")
    c4_1 = crl  (c3_2,  256, [5,5],        name=name+"_C4_1")
    c4_2 = crpl (c4_1,  256, [5,5], [2,2], name=name+"_C4_2")
    c4_2_flattened = tf.reshape (c4_2, [-1, 256 * c4_size * c4_size])
    fc1 = layers.lrelu (layers.fc_layer (c4_2_flattened, 64, name=name+"_FC1"))
    fc2 = layers.fc_layer (fc1, nb_class, name=name+"_FC2")
    return fc2
