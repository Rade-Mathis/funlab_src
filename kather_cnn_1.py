# Computation imports
import math
import numpy as np
import tensorflow as tf

# Utilitary imports
import argparse
import os

# Display import
from PIL import Image

# Personal imports
from Kather_Loader import Kather_Loader as Batch_Loader
from Kather_Apply_Large import Kather_Large_Applier
import layers  # Implementing layers on top of TensorFlow
from Kather_Network import make_network  # Network topographies
import preprocess  # Implemeting preprocessing operation library
import progress  # Little utilitary to make some dynamic display

# File constants
VERSION_MAJOR = 0
VERSION_MINOR = 11
VERSION_MICRO = 0
VERSION_LABEL = "apply"

# Data constant
NB_TRAIN_DATA = 500  # per class
NB_TEST_DATA  = 125  # per class


def get_version (format="number") :
    """ format can be "number", "nickname" or "full" """
    if format == "number" :
        return "{}.{}.{}".format (VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO)
    if format == "nickname" :
        return VERSION_LABEL
    if format == "full" :
        return "{}.{}.{} (\"{}\")".format (VERSION_MAJOR, VERSION_MINOR,
                                           VERSION_MICRO, VERSION_LABEL)
    raise ValueError ("get_version doesn't accept {} format parameter"
                      .format (format))


def dynadisp (iteration, phase, batch_nb, nb_classes, batch_size) :
    images_per_class = 500 if (phase == "train") else 125
    intro = "Iteration {} : {}".format (iteration, phase)
    view_images = (batch_nb - 1) * batch_size
    total_images = nb_classes * images_per_class
    progress.clear_line ()
    progress.print_percent (view_images / total_images, intro)


def choose_eta (eta :str, iteration :int) :
    """ 
    Return a learnig rate based on the asked technique and number of iterations
    """
    if eta == "adaptative" :
        return .001 / (1 + iteration)  # +1 is to prevent 0 divizion
    try :
        return float (eta)
    except :
        raise ValueError ("{} isn't an accepted string, neither a float"
                          .format (eta))


def get_topo (code :str) :
    """ Parse the asked topography to separate nb of layers """
    n = int (code.split ('x')[0])                 # Nb of big-conv layers
    c = int (code.split ('x')[1].split ('_')[0])  # Nb of small-conv layers
    f = int (code.split ('_')[1])                 # Nb of fully connected ones
    return n, c, f


def get_pool (name :str) :
    """ Map a pooling name with the actual function """
    if name == "max" : return tf.nn.max_pool
    elif name == "avg" : return tf.nn.avg_pool
    elif name == "power" : return layers.p_pooling
    else :
        raise ValueError ("{} is not an accepter pooling name".format (name))


# def large_slicer (image, batch_size=1, overlap=0, display_reduce_factor=8) :
#     """
#     Given a big image, will cut it, in several littles

#     Parameters
#     ----------
#     image : PIL.Image
#         The large image to cut.
#     batch_size : int, optional (default: 1)
#         How image per batch in the returned set.
#     overlap : int, optional (default: 0)
#         Considering two neighboring little images, how lines (or columns) of 
#         pixels should they share.
#     display_reduce_factor : int, optional (default: 8)
#         For the display image, how much should we reduce the size of the image.

#     Returns
#     -------
#     A tuple (smalls, display) where :
#     smalls : list<list<numpy.ndarray>> of size [nb_batches, batch_size]
#         The big image but cut in a mozaic of little ones.
#     display : PIL.Image
#         Similar to the input `image` but reduced following the 
#         display_reduce_factor.
#     """
#     small_size = 150
#     too_much_h = (image.size[1] - small_size) % (small_size - overlap)
#     too_much_w = (image.size[0] - small_size) % (small_size - overlap)
#     right_crop  = image.size[0] - math.floor (too_much_w / 2)
#     top_crop    =                 math.floor (too_much_h / 2)
#     left_crop   =                 math.ceil  (too_much_w / 2)
#     bottom_crop = image.size[1] - math.ceil  (too_much_h / 2)
#     croped = image.crop ((left_crop, top_crop, right_crop, bottom_crop))
#     display = image.resize ((image.size[0] // display_reduce_factor,
#                              image.size[1] // display_reduce_factor))
#     matrix = np.array (croped)
#     nb_columns = (matrix.shape[0] - small_size) // (small_size - overlap) + 1
#     nb_lines   = (matrix.shape[1] - small_size) // (small_size - overlap) + 1
#     mosaic = [[]]
#     for i in range (nb_columns) :
#         left = i * (small_size - overlap)
#         right = left + small_size
#         for j in range (nb_lines) :
#             if len (mosaic[-1]) == batch_size :
#                 mosaic.append ([])
#             top = j * (small_size - overlap)
#             bottom = top + small_size
#             mosaic[-1].append (matrix[left:right, top:bottom])
#     return mosaic, display
    

def main (argv) :
    # Set TensorFlow's verbosity
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = str (argv.tf_verbose)
   
    # Setting variables
    nb_classes = len (argv.classes)
    
    # Building a TensorFlow graph #
    # values
    x = tf.placeholder (tf.float32, [None, 150, 150, 3], name="X")
    y = tf.placeholder (tf.float32, [None, nb_classes], name="y")
    eta = tf.placeholder (tf.float32, name="eta")

    # pre-processing
    x_pp = preprocess.preprocesses (x, argv.pre_process, name="Xpp")

    # Network
    n, c, f = get_topo (argv.topography)
    prediction = make_network (x_pp, len (argv.classes), n, f, c,
                               pool=get_pool (argv.pooling[0]),
                               pool_shape=argv.pool_window,
                               pool_stride=argv.pool_stride,
                               pool_arg=float ((argv.pooling + [2])[1]))
    probabilities = tf.nn.softmax (prediction)

    # Learning
    error = tf.reduce_mean (tf.nn.softmax_cross_entropy_with_logits_v2 (
        logits=prediction, labels=y))
    tf.summary.histogram ("loss", error)
    optimizer = tf.train.AdamOptimizer (learning_rate=eta) \
                        .minimize (error)

    # Testing
    correct_prediction = tf.equal (tf.argmax(y, 1),
                                   tf.argmax(probabilities, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # Run the session #
    init_op = tf.global_variables_initializer ()
    summary_op = tf.summary.merge_all ()
    conf = tf.ConfigProto ()
    conf.gpu_options.allow_growth = True
    with tf.Session (config=conf) as session :
        session.run (init_op)

        if argv.logging :
            if tf.gfile.Exists('./board'):
                tf.gfile.DeleteRecursively('./board')
            log_writer = tf.summary.FileWriter('./board',
                                               tf.get_default_graph ())

        saver = tf.train.Saver (max_to_keep=1)
        train_loader = Batch_Loader (argv.classes, "train", argv.pp_dataset,
                                     rotation=argv.rotations)        
        test_loader  = Batch_Loader (argv.classes, "test",  argv.pp_dataset)
        max_acc = 0.
        max_acc_it = 0
        for it in range (argv.nb_iterations) :
            # train
            avg_loss = 0
            train_loader.fill ()
            batch_nb = 0

            while not train_loader.is_empty () :
                batch_nb += 1
                dynadisp (it, "train", batch_nb, nb_classes, argv.batch_size)
                
                batch = train_loader.get_batch (argv.batch_size)
                image, label = batch

                present_eta = choose_eta (argv.eta, it)

                p,loss, _, summary = session.run (
                    [prediction, error, optimizer, summary_op],
                    feed_dict={x : image, y : label, eta : present_eta})

                #print (p)
                if argv.logging :
                    log_writer.add_summary(summary)
                    
                avg_loss += loss * len (batch[1])
            avg_loss /= NB_TRAIN_DATA * nb_classes

            # test
            avg_accuracy = 0
            test_loader.fill ()
            batch_nb = 0
            while not test_loader.is_empty () :
                batch_nb += 1
                dynadisp (it, "test", batch_nb, nb_classes, argv.batch_size)

                batch = test_loader.get_batch (argv.batch_size)
                image, label = batch

                acc, summary = session.run ([accuracy, summary_op],
                                   feed_dict={x:image, y:label})
                avg_accuracy += acc * len (batch[1])

                if argv.logging :
                    log_writer.add_summary(summary)
                
            avg_accuracy /= NB_TEST_DATA * nb_classes
            if avg_accuracy is not None and avg_accuracy > max_acc :
                max_acc_it = it
                max_acc = avg_accuracy
                saver.save (session, "./savings/kather")

            progress.clear_line ()
            print ("Iteration {} : loss = {:.3f}\t acc = {:.3f}"
                   .format (it, avg_loss, avg_accuracy))

        if int (argv.tf_verbose) < 3 :
            print ("Param : {}".format (argv))
            print ("Max acc = {} (it {})".format (max_acc, max_acc_it))

        if argv.application :
            restorr = tf.train.import_meta_graph ("./savings/kather.meta")
            restorr.restore (session, tf.train.latest_checkpoint ("./savings"))
            while True :
                # Ask the user the experiment to run
                choice_i = input (
                    "Choose an image between 1 and 10 (or 'x' to quit) : ")
                if choice_i in ['q', 'x', "quit", "exit", '', ''] :
                    break
                try :
                    choice_i = int (choice_i)
                except :
                    print ("Given input is not an integer")
                    continue
                if choice_i not in range (1, 11) :
                    print ("Given input is not between one and ten")
                    continue

                while True :  # Mode input checking loop
                    choice_o = input ("Choose the overlap (default=0)")
                    if choice_o == "" :
                        choice_o = 0
                    try :
                        choice_o = int (choice_o)
                        break
                    except :
                        print ("{} is not a non-megative int"
                               .format (choice_o))

                app = Kather_Large_Applier (
                    choice_i, argv.batch_size, choice_o)
                while app.still_got_unread_batches () :
                    batch = app.get_one_batch ()
                    labels = session.run (probabilities, feed_dict={x:batch})
                    app.set_one_batch_label (labels)

                app.create_label_image ().show ()
                app.return_display_image ().show ()


    if argv.logging :
        log_writer.close ()

    
if __name__ == "__main__" :
    # Fancy display, 'cause I'm a superficial guy
    print ("~~~ unamed CNN version {} ~~~".format (get_version ("full")))

    # command line argument parser
    parser = argparse.ArgumentParser ()
    parser.add_argument ("-a", "--application", action="store_true",
                         help="Unable the application phase, "
                         "consisting in the display of the classified pixels "
                         "of a big image.", dest="application")
    parser.add_argument ("-b", "--batch-size", default=32, type=int,
                         help="The number of images per batch.",
                         dest="batch_size", metavar="<size>")
    parser.add_argument ("-c", "--classes", nargs='*', default=
                         ["tumor", "stroma", "complex", "lympho", "debris",
                          "mucosa", "adipose", "empty"], type=str,
                         help="The classes to be learned by the network.",
                         dest="classes", metavar="classname")
    parser.add_argument ("-l", "--eta", "--learning-rate", default=".0001",
                         type=str, help="The learning rate of the neurones, "
                         "ie: how fast they will question themselves. The "
                         "value of `x' can either be a float number, or the "
                         "string ``adaptative''", dest="eta", metavar="<x>")
    parser.add_argument ("--lcn", default="raw", type=str, help="Use a "
                         "preprocessed dataset. Just give the name of the "
                         "subfolder.", dest="pp_dataset", metavar="name")
    parser.add_argument ("-n", "--nb-iterations", default=5, type=int,
                         help="How many times will we look at each image.",
                         dest="nb_iterations", metavar="<n>")
    parser.add_argument ("-p", "--preprocess", nargs='*', default=["gcn"],
                         type=str, help="The pre-processing operation(s) that "
                         "should be applied to the data. Can select \"gcn\" ( "
                         "Global Contrast Normalization\"), several or none of"
                         " them.", dest="pre_process", metavar="process")
    parser.add_argument ("--pool", default=["max"], type=str, nargs='*',
                         help="Specify the kind of pooling that will be "
                         "applied, can either be `max', `avg' or `power'. If "
                         "it is power, then it can be followed by a float, "
                         "specifying the power. Default is `max', and power's "
                         "default argument is 2", dest="pooling", metavar="P")
    parser.add_argument ("--pool-window", default=[1,2,2,1], type=int, nargs=4,
                         help="A list of 4 int: the shape of the "
                         "pooling window", dest="pool_window", metavar="s")
    parser.add_argument ("--pool-stride", default=[1,2,2,1], type=int, nargs=4,
                         help="A list if 4 int: the striding of the pooling "
                         "kernel", dest="pool_stride", metavar="d")
    parser.add_argument ("-r", "--rotations", default=[0.], nargs='*',
                         help="Choose how to augment the dataset, with "
                         "rotations: input between 1 and 5 values chosen in : "
                         "[0, 90, 180, 270, \"mirror\"], you can also just "
                         "write \"all\".", dest="rotations", metavar="r")
    parser.add_argument ("-t", "--topography", default="4x1_2", type=str,
                         help="The topology of the network, where `C' is the "
                         "number of CONV layers by layer, `N' is the number "
                         "of conv-relu-pool layers and `F' the number of fully"
                         " conected layers. Available for now : \"3x1_2\", "
                         "\"4x1_2\" and \"4x2_2\"", dest="topography",
                         metavar="NxC_F")
    parser.add_argument ("--tensorboard", default=False, type=bool,
                         help="Should we generate tensorboard's logfiles ?",
                         dest="logging", metavar="bool")
    parser.add_argument ("--verbose", default=1, type=int, help="Set "
                         "tensorflow's verbosity (3 will only show fatal "
                         "errors 2 will add errors, 1 will add warnings, 0 "
                         "will add infos", dest="tf_verbose", metavar="V")
    parser.add_argument ("--version", action="version", version="")
                                     
    # Run da main routine !
    main (parser.parse_args ())
