import pandas as pd
import csv
import sys

def main (filename) :
    in_data = pd.read_csv (filename)
    digits = pd.DataFrame (columns=["name",
                                    "0", "1", "2","3","4","5","6","7","8","9"])
    size = len (in_data)
    i = 0  # dynadisp
    for line in range (size) :
        i += 1  # dynadisp
        name = int (in_data["FileName"][line].split ('.')[0])
        labl = in_data["DigitLabel"][line]
        if not name in digits.index :
            digits.loc[name] = [name] + [False] * 10
        digits.loc[name][(labl % 10) + 1] = True

        # dynadisp
        if i % 1000 == 0 :
            print (end="\rreading: {}%".format (i * 100 // size))

    print ("\rAll {} has been read.")
    print ("Writting new file")

    out_filename = filename.split('.')[0]+"_unmasked.csv"
    print (out_filename)
    digits.to_csv (out_filename, sep=',', index=False)

if __name__ == "__main__" :
    if len (sys.argv) < 2 :
        print ("usage : {} filename.csv".format (sys.argv[0]))
        exit (1)
    main (sys.argv[1])
