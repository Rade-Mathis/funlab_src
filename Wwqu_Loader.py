"""
This is a class to load random batches from the WWQU dataset
"""

from PIL import Image
import numpy as np
import os
import to_borders as tb
import random

VERSION_MAJOR = 0
VERSION_MINOR = 0
VERSION_MICRO = 1
VERSION_LABEL = "how_many"

class Wwqu_Loader :

    IMAGES_SIZES = 416  # Both height and width I tipically want
    
    def __init__ (self, phase="train", folder=None, rotation=[0],
                  borders=False) :
        """
        Create a Wwqu_Loader

        Parameters
        ----------
        phase : str, optional (default: "train")
            With part of the dataset will be used, can be either "train", 
            "test" or "apply".
        folder : str, optional (default: None)
            The localization of the dataset folder, if let to ``None'', will
            try to find the dataset based on what is writter in the 
            ``DATASET_LOCALIZATION'' file. This file should be in the same 
            folder as this python code, composed of a single line: the dataset
            localization. (I recommand giving an absolute path btw.)
        rotation : list, optional (default: [0])
            How to enhance the dataset by randomly rotating the images, the 
            list can hold values in [0, 90, 180, 270, "mirror"]
        borders : not implemented
            Should we extract the gloand borders from the labels ?
            Can be either :
            * False : will return the normal label
            * "border" : will return the border label
            * "both" : will return (image, normal_lbl, borded_lbl)
        """
        # Localise the dataset folder
        if folder is None :
            try :
                self._folder = open ("DATASET_LOCALIZATION").readline ()
            except FileNotFoundError :
                _display_help ("dataset_localization")
                exit (1)
            while self._folder[-1] in ['\n', '\r', ' ', '\t', '\\'] :
                self._folder = self._folder[:-1]
            self._folder += "/ww_qu/"
        else :
            self._folder = folder

        # Prepare an empty structure
        self._pool = []
        
        # Set the dataset phase
        if   phase == "train"            : self._phase = "train"
        elif phase in ["test", "testA"]  : self._phase = "testA"
        elif phase in ["apply", "testB"] : self._phase = "testB"
        else : raise ValueError ("{} is not a known phase".format (phase))

        # Set the augmentation values
        self._get_rotations (rotation)

        # Set the «borderisation»
        if borders in [False, None, "False", "false"] :
            self.border = "none"
        elif borders in [True, "True", "true", "border"] :
            self.border = "border"
        elif borders == "both" :
            self.border = "both"
        else :
            raise ValueError ("{} is not a known border mode".format (borders))
        

    def is_empty (self) :
        return len (self._pool) == 0


    def how_many_labels (self) :
        if self.border == "both" :
            return 2
        return 1


    def fill (self) :
        """ Fill the loader with files, so it can know deliver batches """
        all_files = os.listdir (self._folder)
        self._pool = [i for i in all_files if self._phase in i and "anno" in i]
        for i in range (len (self._pool)) :
            self._pool[i] = self._pool[i].split ('_')[1]


    def get_batch (self, size, items="random") :
        """ Return a batch of size <size> if possible """
        images   = []
        labels   = []
        contours = []
        for i in range (size) :
            # Pick an item
            if items == "random" :
                if self.is_empty () :
                    break
                item = random.choice (self._pool)
                self._pool.remove (item)
            else :
                item = items[i]
            item_name = self._folder + self._phase + '_' + str (item)
            imagename  = item_name + ".bmp"
            annotname  = item_name + "_anno.bmp"
            bordername = item_name + "_border.bmp"

            # Select image properties
            dimentions = (self.IMAGES_SIZES, self.IMAGES_SIZES)
            rotation = random.choice (self._rotations)
            transpose = random.choice ([self._transpose, False])

            # Get images
            image, borders = self.open_img (imagename, dimentions,
                                            rotation, transpose)
            if self.border in ["none", "both"] :
                label, _ = self.open_img (annotname,
                                          borders, rotation, transpose)
                label = np.array (label)
            if self.border in ["border", "both"] :
                contour, _ = self.open_img (bordername,
                                            borders, rotation, transpose)
                contour = np.array (contour)

            # Add to lists
            images.append (np.array (image))
            if self.border != "border" :
                labels.append (np.expand_dims (label, 3))
            if self.border != "none" :
                contours.append (np.expand_dims (contour, 3))

        if self.border == "none"   : return images, labels
        if self.border == "border" : return images, contours
        if self.border == "both"   : return images, labels, contours
                
                
    def _get_rotations (self, rotations) :
        """ Init subroutine to ``parse the rotation list'' """
        self._transpose = False
        if rotations is None or len (rotations) == 0 :
            self._rotations = [0]
        else :
            self._rotations = []
            for i in rotations :
                if i in ["full", "all"] :
                    self._rotations = [0, 90, 180, 270]
                    self._transpose = True
                    return
                if i in ["mirror", "transpose"] :
                    self._transpose = True
                else :
                    try :
                        self._rotations.append (float (i))
                    except  :
                        raise ValueError ("{} isn't an accepted rotation value"
                                          .format (i))

                    
    def open_img (self, filename, crop=None, rotation=0, transpose=False) :
        """
        Open an image, and arndomly crop it if asked
        
        Parameters
        ----------
        crop : tuple ((width, height)) OR ((left, top, right, bottom))
            The size of the outputed image, location is random.
            -OR- The boundaries of the outputed image.
            -OR- None if you want the full image. (default behavior)
        rotation : int, optional (default: 0)
            How degree should we rotate the image ?
        transpose : int, optional (default: False)
            Should we apply a mirror reflection to the image ?

        Returns
        -------
        A tuple (image, boundaries)
        image : PIL.Image
            the croped image
        boundaries : tuple (left, top, right, bottom)
            the boundaries in the initial image

        ToDo
        ----
        Correct the fact that a None crop will actually make it crash
        """
        img = Image.open (filename)
        if not crop is None :
            old_width, old_height = img.size
            if len (crop) == 2 :
                new_width, new_height = crop
                if (new_width > old_width) or (new_height > old_height) :
                    raise ValueError ("Trying to up-crop")
                left = random.randrange (0, old_width - new_width + 1)
                right = left + new_width
                top = random.randrange (0, old_height - new_height + 1)
                bottom = top + new_height
            elif len (crop) == 4 :
                left, top, right, bottom = crop
                if left < 0 or right < left or top < 0 or bottom < top :
                    raise ValueError ("Cropping values doesn't make any sense")
                if right > old_width or bottom > old_height :
                    raise ValueError ("Tring to up-crop")
            else :
                raise ValueError (
                    "crop parameter should be a 2-tuple or 4-tuple")
            img = img.crop ((left, top, right, bottom))
            img = img.rotate (rotation)
        return img, (left, top, right, bottom)            
