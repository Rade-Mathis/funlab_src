""" WIP """
from PIL import Image
import Kather_Loader
import math
import numpy as np

# File constants
VERSION_MAJOR = 0
VERSION_MINOR = 1
VERSION_MICRO = 0
VERSION_LABEL = "release"

class Kather_Large_Applier :
    """ 
    The purpose of this class is to apply the kather_cnn_1 to large Kather
    images.
    """

    def __init__ (self, image_number, batch_size=64, overlap=0,
                  display_reduce_factor=8) :
        """ 
        Create an applier, will alredy cut the image and resize it to make a
        mosaic and a display.

        Parameters
        ----------
        image_number : int
            The number of the large image (ie: filename should be :
            ``.../CRC-Prim-HE-<image_bnumber>_APPLICATION.tiff``)
        batch_size : int, optional (default: 64)
            The size of the batches that will be produced.
        overlap : int, optional (default: 0)
            When creating the mosaic, how many lines (and columns) should be
            shared between a tile and its bottom (and right) neighbor. This
            will obviously augment the size of the output, and hopefully, its
            precision.
        display_reduce_factor : int, optional (default: 8)
            By which factor will both height and width be reduce in the 
            creation of the original image display.        
        """
        # Load the image
        image = Kather_Loader.load_large_image (image_number)

        # Fix some parameters
        small_size = 150
        stride = small_size - overlap

        # Crop the image to make it meet a perfect number of tiles
        too_much_h = (image.size[1] - small_size) % stride
        too_much_w = (image.size[0] - small_size) % stride
        right_crop  = image.size[0] - math.floor (too_much_w / 2)
        top_crop    =                 math.floor (too_much_h / 2)
        left_crop   =                 math.ceil  (too_much_w / 2)
        bottom_crop = image.size[1] - math.ceil  (too_much_h / 2)
        croped = image.crop ((left_crop, top_crop, right_crop, bottom_crop))

        # Get a reduced image for display
        self._display = image.resize ((image.size[0] // display_reduce_factor,
                                       image.size[1] // display_reduce_factor))

        del image  # Try to reduce memory footprint

        # Cut the matrix in little pieces
        matrix = np.array (croped)
        nb_columns = ((matrix.shape[0] - small_size) // stride) + 1
        nb_lines   = ((matrix.shape[1] - small_size) // stride) + 1
        self._mosaic = np.empty ((nb_lines, nb_columns,
                                  small_size, small_size, 3))
        for i in range (nb_columns) :
            left = i * stride
            right = left + small_size
            for j in range (nb_lines) :
                top = j * stride
                bottom = top + small_size
                self._mosaic[i][j] = matrix[left:right, top:bottom]
                #print (np.array(matrix[left:right, top:bottom]).sum ())
        del matrix  # Try to reduce memory footprint

        # Create batches for TensorFlow computation
        self._batches = [[]]
        for i in range (nb_columns) :
            for j in range (nb_lines) :
                if len (self._batches[-1]) == batch_size :
                    self._batches.append ([])
                self._batches[-1].append ((i,j))
        self._read_batches = 0

        # Have a structure ready to recieve labels
        self._labels = np.zeros ((nb_lines, nb_columns, 8))
        self._recieved_labels = 0

    
    def get_one_batch (self, batch_number="auto") :
        """ 
        Return the following batch to treat
        
        Parameter
        ---------
        batch_number : int OR "auto", optional (default: "auto")
            Indicate which batch we want to get. If worth "auto" will just give
            the nth batch, with n being the number of times you called this 
            function.

        Returns
        -------
        A 4-np.ndarray<float> containing a batch of tiles.

        Warnings
        --------
        Its recommended not to use both "auto" and non-automatic call in the
        same code. If you do, be sure to use the `.reset_batchs_counters`
        method decently, or be ready to face a brutal error, or incoherent
        results.
        """
        if batch_number == "auto" :
            batch_number = self._read_batches
        batch_ids = self._batches[batch_number]
        batch = []
        for i in batch_ids :
            batch.append (self._mosaic[i[0]][i[1]])
        self._read_batches += 1
        return np.array (batch)


    def set_one_batch_label (self, labels :list, batch_number="auto") :
        """ 
        Set the calculated labels from a batch in the image maker 

        Paramters
        ---------
        labels : 2-list<floats>
            The labels of the batch to store.
        batch_number : int OR "auto", optional (default: "auto")
            The number of the batch corresponding to the given labels. If 
            "auto", will consider the nth batch, where n is the number of time
            this method as been called.

        Warnings
        --------
        Its recommended not to use both "auto" and non-automatic call in the
        same code. If you do, be sure to use the `.reset_batchs_counters`
        method decently, or be ready to face a brutal error, or incoherent
        results.
        """
        if batch_number == "auto" :
            batch_number = self._recieved_labels
        batch_ids = self._batches[batch_number]
        if len (labels) != len (batch_ids) :
            raise ValueError ("labels seems not to have the good dimention")
        for i in range (len (batch_ids)) :
            x = batch_ids[i][0]
            y = batch_ids[i][1]
            self._labels[x, y, :labels[i].size] = labels[i]
        self._recieved_labels += 1


    def reset_batchs_counters (self, batch_number=0) :
        """ 
        Used to reset counter for batch getter and label setter 

        Parameter
        ---------
        batch_number : int, optional (default: 0)
            The next batch getter and setter will work with.

        Warning
        -------
        Please just don't mess with the Zohan ok ?...
        """
        self._read_batches    = batch_number
        self._recieved_labels = batch_number

    def still_got_unread_batches (self) :
        """ 
        Return True if there still are batches to read with "auto" mode.
        """
        return self._read_batches != len (self._batches)

    
    def create_label_image (self, mode="dummy") :
        """ 
        After receiving all the labels, create a simple color image based on 
        labels. Size will depend on the number of labels.

        Parameter
        ---------
        mode : str
            This is a future work, actually totally useless. I recommand not
            using it, or filling it with any quote from a Robin Hobb book, 
            since it will permit your code to look very nice, deep, and dark.

        Returns
        -------
        A PIL.Image
        """
        labels = np.array (self._labels)
        output = labels.argmax (2)
        COLORS = np.array ([[0xff, 0x66, 0x00],  # orange
                            [0x00, 0x99, 0x00],  # green
                            [0xff, 0xcc, 0x00],  # yellow
                            [0x00, 0x99, 0xff],  # blue
                            [0xcc, 0x00, 0xcc],  # purple
                            [0xff, 0xcc, 0xcc],  # palid
                            [0x77, 0x77, 0x77],  # grey
                            [0xee, 0xee, 0xee]]) # black
        output = COLORS[output]  # some numpy magic <3
        return Image.fromarray (output.astype (np.uint8))


    def return_display_image (self) :
        """ Returns a PIL.Image, a downsized version of the large image """
        return self._display
