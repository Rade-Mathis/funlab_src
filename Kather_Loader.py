from PIL import Image
import numpy as np
import os
import random


VERSION_MAJOR = 1
VERSION_MINOR = 0
VERSION_MICRO = 1
VERSION_LABEL = "standardization_wwqu"


class Kather_Loader :
    """
    Create with myloader = KatherLoader (["classname 1", "classname 2", ...]")
    Fill the pool with myloader.fill ()
    Get a batch with myloader.get_batch (desire_size_of_the_batch)
    """
    # Dataset constants
    CLASS_FOLDER  = {"tumor"   : "01_TUMOR/",      "stroma" : "02_STROMA/",
                     "complex" : "03_COMPLEX/",    "lympho" : "04_LYMPHO/",
                     "debris"  : "05_DEBRIS/",     "mucosa" : "06_MUCOSA/",
                     "adipose" : "07_ADIPOSE/",    "empty"  : "08_EMPTY/"}

    def __init__ (self, classes, phase="train", preprocess="raw",
                  folder=None, rotation=[0.]) :
        """ 
        rotation is made to enhance the data set, note that it
        won't augment its size: when picking an image, it will just randomly
        choose to rotate or not the image.
        rotations should contains only number, or "mirror", or "all"
        NB : choosing rotations that are not multiples of 90 is a bad idea
        """
        # Localize the datasets folder
        if folder is None :
            try :
                self.folder = open ("DATASET_LOCALIZATION").readline ()
            except FileNotFoundError :
                _display_help ("dataset_localization")
                exit (1)
            while self.folder[-1] in ['\n', '\r', ' ', '\t', '\\'] :
                self.folder = self.folder[:-1]
            self.folder += "/kather"
        else :
            self.folder = folder    
                
        # Choose the good subfolder
        if phase in ["train", "test"] :
            self.folder += "/small/" + phase
        else :
            raise ValueError ("{} is not a known phase".format (phase))
        self.folder += '/' + preprocess + '/'

        # Prepare some empty structures
        self.classes = classes
        self.pool = {}
        for i in classes :
            if i not in self.CLASS_FOLDER.keys () :
                raise ValueError ("{} is not a known classname".format (i))
            self.pool[i] = []

        # The the dataset augmentation parameters
        self._get_rotations (rotation)
        

    def is_empty (self) :
        for i in self.pool.values () :
            if len (i) > 0 :
                return False
        return True

    def available_classes (self) :
        out = []
        for i in self.pool.keys () :
            if len (self.pool[i]) > 0 :
                out.append (i)
        return out

    def fill (self) :
        """ Put all asked in a pool, ready to be batched and loaded """
        for i in self.classes :
            folder = self.folder + self.CLASS_FOLDER [i]
            self.pool[i] = os.listdir (folder)

    def get_batch (self, size) :
        """ 
        Return a batch of <size> images from the pool, and remove them from the
        pool.
        """
        labels   = []
        matrices = []
        for i in range (size) :
            if self.is_empty () :
                break
            classname = random.choice (self.available_classes ())
            labels.append (np.zeros (len (self.classes)))
            labels[i][self.classes.index (classname)] = 1
            image_name = random.choice (self.pool[classname])
            image = Image.open (
                self.folder + self.CLASS_FOLDER [classname] + image_name)
            image = image.rotate (random.choice (self.rotations))
            if self.transpose and random.choice ([True, False]) :
                image = image.transpose (Image.TRANSPOSE)
            matrices.append (np.array (image))
            self.pool[classname].remove (image_name)
        return matrices, labels


    def _get_rotations (self, rotations) :
        self.transpose = False
        if rotations is None or len (rotations) == 0 :
            self.rotations = [0.]
        else :
            self.rotations = []
            for i in rotations :
                if i in ["full", "all"] :
                    self.rotations = [0, 90, 180, 270]
                    self.transpose = True
                    return
                if i in ["mirror", "transpose"] :
                    self.transpose = True
                else :
                    try :
                        self.rotations.append (float (i))
                    except  :
                        raise ValueError ("{} isn't an accepted rotation value"
                                          .format (i))


def _display_help (self, h) :
    if h == "dataset_localization" :
        print ("""
Cannot find the ``DATASET_LOCALIZATION'' file, maybe you just didn't created
one : create a file named ``DATASET_LOCALIZATION'' in the source folder (the
one containing the python scripts), this file should just contain one line : 
the pathname of kather dataset root folder, such as :
`` /home/username/path/to/kather/dataset/small ''
        """)


def load_large_image (number :int, folder=None) :
    """
    Load a large image from Kather's dataset, for application stage purpose
    """
    if folder is None :
        try :
            folder = open ("DATASET_LOCALIZATION").readline ()
        except FileNotFoundError :
            _display_help ("dataset_localization")
            exit (1)
        while folder[-1] in ['\n', '\r', ' ', '\t', '\\', '/'] :
            folder = folder[:-1]

    return Image.open ("{}/large/CRC-Prim-HE-{:0>2}_APPLICATION.tif"
                       .format (folder, number))
