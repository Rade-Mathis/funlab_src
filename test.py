import tensorflow as tf
import sys

def reshape () :
    a = tf.placeholder (tf.float32, [None])
    b = tf.reshape (a, [1, -1])
    print (a,b)
    init_op = tf.global_variables_initializer ()
    with tf.Session () as s :
        s.run (init_op)
        print (s.run (a, feed_dict={a: [1,2,3]}).shape)
        print (s.run (b, feed_dict={a: [1,2,3]}).shape)

def exp_dim () :
    a = tf.placeholder (tf.float32, [None, 2])
    b = tf.expand_dims (a, 0)
    print (a,b)
    init_op = tf.global_variables_initializer ()
    with tf.Session () as s :
        s.run (init_op)
        # print (s.run (a, feed_dict={a: [1,2,3]}).shape)
        print (s.run (b, feed_dict={a: [[1,2], [11,22], [111,222]]}).shape)

def threads () :
    ROOT_FOLDER = "/home/rademathis/work"
    
    def do_thread () :
        filename_queue = tf.train.string_input_producer (
            [ROOT_FOLDER + "/datasets/housenumbers_full/train/{}.png".format (
                1)])
        reader = tf.WholeFileReader ()
        _, image_file = reader.read (filename_queue)
    
        coord = tf.train.Coordinator ()
        threads = tf.train.start_queue_runners (coord=coord)
        coord.request_stop ()
        coord.join (threads)
        return 42
        
    init_op = tf.global_variables_initializer ()
    with tf.Session () as s :
        s.run (init_op)
        print (do_thread ())
        print (do_thread ())

def mult_img_read () :
    filename_queue = tf.train.string_input_producer(
        tf.train.match_filenames_once(
            "/home/rademathis/work/datasets/housenumbers_full/train/*.png"))
    print ("###", filename_queue.size())
    image_reader = tf.WholeFileReader()
    _, image_file = image_reader.read(filename_queue)
    image = tf.image.decode_png(image_file)
    init_op = tf.global_variables_initializer ()
    with tf.Session() as sess :
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)
        image_tensor = sess.run([image])
        print(image_tensor)
        coord.request_stop()
        coord.join(threads)

def copy () :
    t1 = tf.Variable ([[1,2],[11,22]])
    # t1.assign ([[3,4],[33,44]])
    t1 = tf.Variable ([[3,4],[33,44]])
    # t2 = tf.Variable (t1.initialized_value ())
    # t2 = tf.identity (t1)
    t2 = t1
    # t1.assign ([[5,6],[55,66]])
    t1 = tf.Variable ([[5,6],[55,66]])
    init_op = tf.global_variables_initializer ()
    with tf.Session () as s :
        s.run (init_op)
        print (s.run (t1))
        print (s.run (t2))

def shapes () :
    t = tf.placeholder (tf.float32, [None, None])
    s = tf.shape (t)
    p = (s[1])
    print (type (s), s)
    print ("p=",p)
    init_op = tf.global_variables_initializer ()
    with tf.Session () as sess :
        sess.run (init_op)
        print (sess.run (s, feed_dict={t: [[1,2,3],[11,22,33]]}))
        print (type (sess.run (s, feed_dict={t: [[1,2,3],[11,22,33]]})))
        print ("p = ", sess.run (p, feed_dict={t: [[1,2,3],[11,22,33]]}))

def ctrlD () :
    print ("")
        
if __name__ == "__main__" :
    if len (sys.argv) < 2 :
        print ("usage: {} test-name".format (sys.argv[0]))
    elif sys.argv[1] == "reshape" :
        reshape ()
    elif sys.argv[1] == "exp_dim" :
        exp_dim ()
    elif sys.argv[1] == "threads" :
        threads ()
    elif sys.argv[1] == "mult_img_read" :
        mult_img_read ()
    elif sys.argv[1] == "copy" :
        copy ()
    elif sys.argv[1] == "shapes" :
        shapes ()
    elif sys.argv[1] == "ctrlD" :
        ctrlD ()
    else :
        print ("Unknown routine")
