"""
Ici on va tenter de faire un FCN qui trouve les glandes du dataset Warwick-QU,
Comme le challenge GLAS

TODO :
- try with an activation function in c5
- save context of best F-score to prevent overfitting
- add a transposed convolution to obtain pixel-wise labels
- 5x5 windows are nice, but what about 7x7 ? Or more ?
- augment dataset by rotating pictures by 90, 180 and 270 degrees
- try to create a lcn
- (idea) a boosting variable for each class
- (aes) display convolution image

Discuss and results :
- Doesn't seem to train anymore after 8 iterations 
- No F-measure difference between 5C and 5x2C
"""
# Computation imports
import numpy as np
import tensorflow as tf

# Image treatement imports
from PIL import Image

# Common util imports
import argparse
import random
import sys
import progress

FILE_FOLDER = "/home/rademathis/work/datasets/ww_qu"
NB_TRAIN = 85  # from 1 to 85
NB_TEST  = 60  # from 1 to 60

def to_lists (image) :
    """
    Take a PIL.Image, and cast it into a bi-dimensional list
    """
    width, heigh = img.size
    return [[ img.getpixel ((x, y)) \
              for x in range (width)] for y in range (heigh)]

def open_train_img (img_number, crop=None) :
    """ Shortcut, see open_img """
    return open_img (FILE_FOLDER + "/train_" + str (img_number) + ".bmp", crop)

def open_train_lbl (lbl_number, crop=None) :
    """ Shortcut, see open_img """
    return open_img (FILE_FOLDER + "/train_" + str (lbl_number) + "_anno.bmp",
                     crop)

def open_test_img (img_number, crop=None) :
    """ Shortcut, see open_img """
    return open_img (FILE_FOLDER + "/testA_" + str (img_number) + ".bmp", crop)

def open_test_lbl (lbl_number, crop=None) :
    """ Shortcut, see open_img """
    return open_img (FILE_FOLDER + "/testA_" + str (lbl_number) + "_anno.bmp",
                     crop)

def open_img (filename, crop=None) :
    """
    Open an image, and arndomly crop it if asked

    Parameters
    ----------
    crop : tuple ((width :int, height :int)) OR ((left, top, right, bottom))
        the size of the outputed image, location is random 
        OR the boundaries of the outputed image

    Returns
    -------
    A tuple (image, boundaries)
    image : PIL.Image
        the croped image
    boundaries : tuple (left, top, right, bottom)
        the boundaries in the initial image
    """
    img = Image.open (filename)
    if not crop is None :
        old_width, old_height = img.size
        if len (crop) == 2 :
            new_width, new_height = crop
            if (new_width > old_width) or (new_height > old_height) :
                raise ValueError ("Trying to up-crop")
            left = random.randrange (0, old_width - new_width + 1)
            right = left + new_width
            top = random.randrange (0, old_height - new_height + 1)
            bottom = top + new_height
        elif len (crop) == 4 :
            left, top, right, bottom = crop
            if left < 0 or right < left or top < 0 or bottom < top :
                raise ValueError ("Cropping values doesn't make any sense")
            if right > old_width or bottom > old_height :
                raise ValueError ("Tring to up-crop")
        else :
            raise ValueError ("crop parameter should be a 2-tuple or 4-tuple")
        img = img.crop ((left, top, right, bottom))
    return img, (left, top, right, bottom)            


def lrelu (x, alpha=.01) :
    return tf.nn.relu(x) - alpha * tf.nn.relu(-x)

def conv_relu_layer (in_data, nb_filters, filter_shape, relu=True, name=None):
    """ 
    Create a convolutional layer, followed by a non-linear activation one.

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    # weights : string
    #     name of a numpy csv-style saved array
    #     will be used to initialize the weigths
    #     if `weigths == "random"` then they will be randomly generated
    #     By default, `weights` is "random"
    name : string, optional
        name of the return tensorflow's object

    Returns
    -------
    A new tf CONV layer
    """
    # setups
    w_name = None if name is None else name + "_W"
    b_name = None if name is None else name + "_b"
    f_name = None if name is None else name + "_f"
    if len (in_data.shape) == 3 :
        in_data_reshaped = tf.expand_dims (in_data, 0)
    else :
        in_data_reshaped = tf.identity (in_data)
    nb_in_channels = int (in_data_reshaped.shape[3])
    conv_shape = [filter_shape[0], filter_shape[1], nb_in_channels, nb_filters]

    # Trainable tensors
    weights = tf.Variable (  # todo : tune the mean and stddev
        tf.truncated_normal (conv_shape, mean=0., stddev=.01),
        name=w_name)
    bias = tf.Variable (  # todo : idem
        tf.truncated_normal ([nb_filters], mean=0., stddev=.01),
        name=b_name)

    # Compute
    output = tf.nn.conv2d (in_data_reshaped, weights,
                           [1,1,1,1], padding="SAME")
    output += bias

    # Activation function
    if relu: 
        output = lrelu (output)
        #output = tf.nn.relu (output, name=f_name)
        #output = tf.nn.softplus (output, name=f_name)
    return output


def conv_relu_pool_layer (in_data, nb_filters, filter_shape, pool_shape,
                          pooling=tf.nn.max_pool, relu=True, name=None) :
    """ 
    Create a typical 'full CONV layer': CONV-ReLU-POOL

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    pool_shape : list
        the classical (1 and 2) dimension of the pooling window
    pooling : function
        the kind of applyed pooling
        By default, we apply a max-pooling
    name : string, optional
        name of the returned tensorflow's object

    Returns
    -------
    A tf opreation performing a CONV-ReLU-maxpool operation layer.
    """
    c_name = "conv" if name is None else name + "_conv"
    p_name = "pool" if name is None else name + "_pool"
    conv_out = conv_relu_layer (in_data, nb_filters, filter_shape, relu,
                                c_name)
    ksize = [1, pool_shape[0], pool_shape[1], 1]  # question it again
    strides = [1, pool_shape[0], pool_shape[1], 1]  # question it also
    return pooling (conv_out, ksize=ksize, strides=strides, padding="SAME",
                    name=p_name)



# Some networks implementations
def create_network_5C (in_dt, name="5C") :
    """ Create a 5 fully convolutional layer network """
    c1 = conv_relu_pool_layer (in_dt, 64, [5,5], [2,2], relu=True, name="C1")
    c2 = conv_relu_pool_layer (c1,   128, [5,5], [2,2], relu=True, name="C2")
    c3 = conv_relu_pool_layer (c2,   256, [5,5], [2,2], relu=True, name="C3")
    c4 = conv_relu_pool_layer (c3,   512, [5,5], [2,2], relu=True, name="C4")
    return conv_relu_layer    (c4,     2, [5,5],        relu=True, name="C5")

def create_network_5x2C (in_dt, name="5x2C") :
    """ Create a 5*2 fully convolutional layer network """
    c11 = conv_relu_layer    (in_dt,  64, [5,5],        relu=True, name="C11")
    c12 = conv_relu_pool_layer (c11, 128, [5,5], [2,2], relu=True, name="C12")
    c21 = conv_relu_layer      (c12, 128, [5,5],        relu=True, name="C21")
    c22 = conv_relu_pool_layer (c21, 128, [5,5], [2,2], relu=True, name="C21")
    c31 = conv_relu_layer      (c22, 256, [5,5],        relu=True, name="C31")
    c32 = conv_relu_pool_layer (c31, 256, [5,5], [2,2], relu=True, name="C32")
    c41 = conv_relu_layer      (c32, 512, [5,5],        relu=True, name="C41")
    c42 = conv_relu_pool_layer (c41, 512, [5,5], [2,2], relu=True, name="C42")
    return conv_relu_layer     (c42,   2, [5,5],        relu=True, name="C5")


def loss (logits, labels, num_classes, head=None) :
    """
    Calculate the loss from the logits and the labels.
    
    Parameters
    ----------
    logits : tensor, float - [batch_size, width, height, num_classes].
    labels : Labels tensor, int32 - [batch_size, width, height, num_classes].
        The ground truth of your data.
    num_classes : int
    head : numpy array - [num_classes]
        Weighting the loss of each class
        Optional: Prioritize some classes

    Returns
    -------
    Loss tensor of type float.

    Source
    ------
    MarvinTeichmann@GitHubUser | MIT Licence
    """
    with tf.name_scope('loss'):
        logits = tf.reshape(logits, (-1, num_classes))
        epsilon = tf.constant(value=1e-4)
        labels = tf.to_float(tf.reshape(labels, (-1, num_classes)))

        softmax = tf.nn.softmax(logits) + epsilon

        if head is not None:
            cross_entropy = - tf.reduce_sum (
                tf.multiply (labels * tf.log (softmax), head),
                reduction_indices=[1])
        else:
            cross_entropy = - tf.reduce_sum (
                labels * tf.log (softmax),
                reduction_indices=[1])

        cross_entropy_mean = tf.reduce_mean (cross_entropy,
                                             name='xentropy_mean')
        tf.add_to_collection('losses', cross_entropy_mean)

        loss = tf.add_n(tf.get_collection('losses'), name='total_loss')
    return loss


def get_batch (batch_number, batch_size, phase, shift=0) :
    """
    Return a batch of image and labels

    Parameters
    ----------
    batch_number : int
    batch_size : int
    phase : str
        can either be "train" or "test"
    
    Returns
    -------
    2 values: image_batch and label_batch
    """
    image_batch = []
    label_batch = []
    for i in range (1, batch_size + 1) :
        img_nb = batch_number * batch_size + i + shift
        if phase == "train" :
            img, window = open_train_img (img_nb, (416, 416))
            lbl, _      = open_train_lbl (img_nb, window)
        elif phase == "test" :
            img, window = open_test_img (img_nb, (416, 416))
            lbl, _      = open_test_lbl (img_nb, window)
        else :
            raise ValueError ("Unknown phase {}".format (phase))
        image_batch.append (np.array (img))
        label_batch.append (np.array (lbl).reshape ((416, 416, 1)))
    return image_batch, label_batch


def main (args) :
    # Some usefull constant
    batch_size = 5  # I've no gpu and size vary, so ... dunno
    fixdishit = np.empty ((batch_size, 416//16, 416//16, 1))
    fixdishit.fill (.5)

    # Load data
    x = tf.placeholder (tf.float32, [None, 416, 416, 3], name="x")
    y = tf.placeholder (tf.float32, [None, 416, 416, 1], name="y")

    # Only compute one value by c5's pixel
    x_gcn = tf.map_fn (lambda img : tf.image.per_image_standardization (img),
                       x, parallel_iterations=8)
    logits = create_network_5C (x_gcn)

    # Compute a pooled answer to match logits dimensions
    y_p = tf.nn.avg_pool (tf.sign (y),
                          ksize=[1,16,16,1], strides=[1,16,16,1],
                          padding="SAME", name="y_p")
    y_rshp = tf.reshape (y_p, [-1, 416//16, 416//16])
    y_bin = tf.cast (y_rshp > .5, tf.int64)
    y_1hot = tf.one_hot (y_bin, 2)
    
    # Compute error
    error = loss (logits, y_1hot, 2)
    optimizer = tf.train.AdamOptimizer (learning_rate=args.eta) \
                        .minimize (error)
    
    # evaluate success # TODO, refaire ca
    prediction = tf.argmax (tf.nn.softmax (logits), axis=3)
    px_wise_success = tf.cast (tf.equal (prediction, y_bin), tf.int64)
    px_wise_failure = tf.negative (px_wise_success - 1)
    negatives = tf.negative (prediction - 1)
    true_pos_x2 = tf.reduce_sum (tf.multiply (prediction, px_wise_success)) * 2
    false_pos   = tf.reduce_sum (tf.multiply (prediction, px_wise_failure))
    false_neg   = tf.reduce_sum (tf.multiply (negatives,  px_wise_failure))
    f1_score = tf.cond (tf.equal (true_pos_x2, 0),
                        lambda : tf.cast (0., tf.float64),
                        lambda : tf.divide (
                            true_pos_x2,
                            true_pos_x2 + false_pos + false_neg))

    # Run the session
    init_op = tf.global_variables_initializer ()
    with tf.Session () as session :
        session.run (init_op)
        
        for iter in range (args.iterations) :
            print ("iteration {}/{}".format (iter + 1, args.iterations))

            # Training phase
            avg_err = 0
            for b in range (NB_TRAIN // batch_size) :
                image_batch, label_batch = get_batch (b, batch_size, "train")
                err, _ ,logi= session.run ([error, optimizer, logits],
                                      feed_dict={ x: image_batch,
                                                  y: label_batch })
                avg_err *= b
                avg_err += err
                avg_err /= b + 1
                progress.clear_line ()
                print (end="training batch {}/{}\terr = {:.3f}"
                       .format (b+1, NB_TRAIN // batch_size, avg_err))
                sys.stdout.flush ()
            print ()
                
            # Testing phase
            f1 = 0
            for b in range (NB_TEST // batch_size) :
                progress.clear_line ()
                print (end="testing batch {}/{}\t"
                       .format (b+1, NB_TEST // batch_size))
                sys.stdout.flush ()
                image_batch, label_batch = get_batch (b, batch_size, "test")
                f1 += session.run (f1_score, feed_dict={ x: image_batch,
                                                         y: label_batch })
            f1 /= (NB_TEST // batch_size)
            print ("F1-score = {:.3f}".format (f1))

        # Manual testing phase
        keep_going = True
        while (keep_going) :
            test_img_nb = input ("Enter an image number : ")
            if test_img_nb in ['x','e', 'q', "exit", "quit", '', ''] :
                break
            try :
                test_img_nb = int (test_img_nb)
            except :
                print ("Dat iz no numberz")
                continue
            if test_img_nb < 1 or test_img_nb >= 60 :
                print ("between 1 and 59")
                continue

            # Get image and label
            image, label = get_batch (0, 1, "test", test_img_nb)

            # Compute
            ypooled, pred, tp, fp, fn, fscore = np.array (
                session.run ([y_bin, prediction,
                              true_pos_x2, false_pos, false_neg, f1_score] , 
                             feed_dict={x:image, y:label}))
            print ("F1-score : {}".format (fscore))

            # Some boaring treatement
            img_image = image[0]
            img_label = label[0]
            ypooled = ypooled[0]
            pred = pred[0]
            img_label = img_label.reshape((416,416)).astype ("uint8")
            img_label *= 25
            ypooled *= 200
            pred *= 200            
            img_ypooled = Image.fromarray (ypooled.astype (np.uint8))          
            img_pred = Image.fromarray (pred.astype (np.uint8))
            # Display images
            Image.fromarray (img_image).show ()
            Image.fromarray (img_label).show ()
            img_ypooled.resize ((416,416)).show ()
            img_pred.resize ((416,416)).show ()

                    
if __name__ == "__main__" :
    parser = argparse.ArgumentParser ()
    parser.add_argument ("-η", "--eta", "--learning-rate", default=.0001,
                         type=float, help="The perceptrons' learning rate",
                         dest="eta")
    parser.add_argument ("-n", "--nb-iterations", default=12, type=int,
                         help="How many learning iterations on the dataset",
                         dest="iterations")
    parser.add_argument ("-s", "--threshold", default=.5, type=float,
                         help="Minimum probability to consider the pixel as a "
                         +"gland", dest="threshold")
    main (parser.parse_args())
