from PIL import Image
import numpy as np
import sys

if __name__ == "__main__" :
    if len (sys.argv) < 2 :
        print ("gimme a filename ya sucka !")
    img = np.array (Image.open (sys.argv[1]))

    binary = (img != 0).astype (np.uint8)
    revers = (binary == 0).astype (np.uint8)
    pos = binary.sum ()
    neg = revers.sum ()

    print ("pos : {}  |  neg : {}".format (pos, neg))
    print ("ratio : {:.3f}".format (pos / (pos + neg)))
    
else :
    print ("this file isn't to be included somewhere ... at your risks so")    
