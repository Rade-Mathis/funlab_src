import math
import sys

def print_percent (value, intro="", place=79) :
    using = place - len (intro) - 7
    dieses = math.floor (value * using)
    digit = str (math.floor (10 * (value * using - dieses)))

    print (end=intro)
    print (end=" ")
    for i in range (dieses) :
        print (end="#")
    print (end=digit)
    for i in range (using - dieses) :
        print (end=" ")
    print (end="{:>3} %".format (round (value * 100)))
    sys.stdout.flush ()

def clear_line (place=79) :
    print (end="\r")
    for i in range (place) :
        print (end=" ")
    print (end="\r")
