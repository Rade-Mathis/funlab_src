## Licence
All the files present in this repository are under
[GPL3](https://www.gnu.org/licenses/gpl-3.0.txt). A full text of this licence
can be found in the file named `LICENCE.txt`.

## Main files
- cnn_kather_1.py --> my classification of the kather dataset
- fcn_wwqu_v3.py  --> my segmantation if the WW-QU dataset

## About
All this codes have been written by
[RadeMathis](https://orcid.org/0000-0002-6150-3555). During his internship at
the Chang Gung University, for his master's degree from the university of
Lyon 1.

The objective is to make some research, hopefully usefull for colorectal cancer
diagnostics.

## Usage

These scripts require `Python3` and `TensorFlow`.

- The dataset for the CNN is here: https://zenodo.org/record/53169
- The one for the FCN is here: https://warwick.ac.uk/fac/sci/dcs/research/tia/glascontest/download/

Put them where you want and name the repositories `kather/` and `ww_qu/`.

In the same repositories where you will store the python scripts,
create a file named `DATASET_LOCALIZATION`, and write the name of the directory
where you stored the dataset inside.

It means you must have a directory called `/somewhere/on/your/computer/kather`
and/or one called `/somewhere/on/your/computer/ww_qu`. And do :

```bash
cd /an/other/place/funlab_src/
echo "/somewhere/on/your/computer" > DATASET_LOCALIZATION
```

Then launch either `python3 cnn_kather_1.py --help` or
`python3 fcn_wwqu_v3.py --help` to get some information on how to use the
scripts.

## Contact

If any information is needed, if something that should work doesn't, send me an
e-mail at: "mathis[dot]rade-morzadec[at]etu[dot]univ-lyon1[dot]fr".