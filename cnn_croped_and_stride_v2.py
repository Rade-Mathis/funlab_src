# TODO :
# Changer les fichiers d'entree (ca veut dire, preparer des csv)
"""
This file tries to build a CNN that will train on the croppped GSVHN dataset,
and used on the non-croped one as a slidding windows in order to localize the
secified number 
"""

# Classical imports
import argparse
import csv
import sys

# Image import
from PIL import Image

# Computation/technical imports
import numpy as np
import scipy.io as sio
import tensorflow as tf

# Personnal import
import progress as util

ROOT_FOLDER = "/home/rademathis/work"
TRAIN_FILE = ROOT_FOLDER \
                    + "/datasets/housenumbers_cropped/train_32x32.mat"
TEST_FILE  = ROOT_FOLDER \
                    + "/datasets/housenumbers_cropped/test_32x32.mat"

verbose = True

def open_png (filename) :
    """ 
    Open a PNG file as an image tensor - may not be an optimal function
    """
    img = Image.open (filename)
    # width, heigh = img.size
    # return [[ img.getpixel ((x, y)) \
    #           for x in range (width)] for y in range (heigh)]
    return img
    
def open_train_png (img_number) :
    return open_png (
        ROOT_FOLDER + "/datasets/housenumbers_full/train/{}.png".format (
            img_number))

def open_test_png (img_number) :
    return open_png (
        ROOT_FOLDER + "/datasets/housenumbers_full/test/{}.png".format (
            img_number))


def contrast_normalization (in_data, local_kernel_size=None) :
    """
    Apply a local and global contrast normalization on an image

    Paremeters
    ----------
    in_data : numpy.ndarray
        The image to normalize
    local_kernel_size : int
        Size of the square slidding window that is going to be applied as local
        contrast normalization.
        By default, local_kernel_size is `None`, meaning it will only apply a
        global contrast normalization.

    Returns
    -------
    A numpy.ndarray of the normalized image
    """
    return in_data
# don,t make any normalization, my shity implementation is way too slow
    # if local_kernel_size is None :
    #     return gcn (in_data)
    # return gcn (lcn (in_data, local_kernel_size))


def gcn (in_data) :
    """
    Global contrast normalization subroutine
    """
    l_data = in_data.sum (2)  # R+G+B on each pixel
    l_min = l_data.min ()
    l_max = l_data.max ()
    new_l_data = (l_data - l_min) * 765 / (l_max + 1 - l_min)  # 765 = 3 * 255
    out_data = np.empty (in_data.shape)
    for i in range (3) :
        out_data[:,:,i] = in_data[:,:,i] * new_l_data / (l_data + 1)
    return out_data


def lcn (in_data, kernel_size) :
    """
    Local contrast normalization subroutine
    """
    x, y, c = in_data.shape
    out_data = np.empty ((x,y,c))
    for i in range (x) :
        for j in range (y) :
            patch = np.empty ((kernel_size, kernel_size, c))
            for k in range (kernel_size) :
                ii = i + k - (kernel_size // 2)
                for l in range (kernel_size) :
                    jj = j + l - (kernel_size // 2)
                    if (0 <= ii < x) and (0 <= jj < y) :
                        # print ('ICI', ii, jj, type(ii), type(jj))
                        patch[k][l] = in_data[ii][jj]
                    else :
                        patch[k][l] = in_data[i][j]
            patch = gcn (patch)
            out_data[i,j] = patch[kernel_size // 2, kernel_size // 2]
    return out_data
    

def conv_relu_layer (in_data, nb_filters, filter_shape, name=None) :
    """ 
    Create a convolutional layer, followed by a non-linear activation one.

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    # weights : string
    #     name of a numpy csv-style saved array
    #     will be used to initialize the weigths
    #     if `weigths == "random"` then they will be randomly generated
    #     By default, `weights` is "random"
    name : string, optional
        name of the return tensorflow's object

    Returns
    -------
    A new tf CONV layer
    """
    # setups
    w_name = None if name is None else name + "_W"
    b_name = None if name is None else name + "_b"
    f_name = None if name is None else name + "_f"
    if len (in_data.shape) == 3 :
        in_data_reshaped = tf.expand_dims (in_data, 0)
    else :
        in_data_reshaped = tf.identity (in_data)
    nb_in_channels = int (in_data_reshaped.shape[3])
    conv_shape = [filter_shape[0], filter_shape[1], nb_in_channels, nb_filters]

    # Trainable tensors
    weights = tf.Variable (  # todo : tune the mean and stddev
        tf.truncated_normal (conv_shape, mean=0., stddev=.05),
        name=w_name)
    bias = tf.Variable (  # todo : idem
        tf.truncated_normal ([nb_filters], mean=0., stddev=1.),
        name=b_name)

    # Compute
    output = tf.nn.conv2d (in_data_reshaped, weights,
                           [1,1,1,1], padding="SAME")
    output += bias
    output = tf.nn.relu (output, name=f_name)
    return output


def conv_relu_pool_layer (in_data, nb_filters, filter_shape, pool_shape,
                          name=None) :
    """ 
    Create a typical 'full CONV layer': CONV-ReLU-POOL

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    nb_filters : int
        How many filters in //
    filter_shape : list
        heigh and width of the filters
    pool_shape : list
        the classical (1 and 2) dimension of the pooling window
    name : string, optional
        name of the returned tensorflow's object

    Returns
    -------
    A tf opreation performing a CONV-ReLU-maxpool operation layer.
    """
    c_name = "conv" if name is None else name + "_conv"
    p_name = "pool" if name is None else name + "_pool"
    conv_out = conv_relu_layer (in_data, nb_filters, filter_shape, c_name)
    ksize = [1, pool_shape[0], pool_shape[1], 1]  # question it again
    strides = [1, 2, 2, 1]  # question it also
    return tf.nn.max_pool (conv_out, ksize=ksize, strides=strides,
                           padding="SAME", name=p_name)


def fc_layer (in_data, out_size: int, mean=0., stddev=.02, name=None) :
    """
    A tipical fully connected layer of perceptrons.

    Parameters
    ----------
    in_data : 2-Tensor (batch, nb_inputs)
        The input data (X, or previous layer's output)
    out_size : int
        How many outputed features do you want, ie: how many neurones in the 
        layer.
    mean : float, optional
        Weigth initialization.
    stddev : float, optional
        Weigth initialization.
    name : string, optional
        name of the returned TensorFlow object.

    Returns
    -------
    A TensorFlow object performing a fully connected layer operation.
    """
    w_name = None if name is None else name+"_w"
    b_name = None if name is None else name+"_b"
    nb_inputs = int (in_data.shape[1])
    
    w = tf.Variable (tf.truncated_normal ([nb_inputs, out_size],
                                          mean=mean, stddev=stddev,
                                          name=w_name))
    b = tf.Variable (tf.truncated_normal ([out_size],
                                          mean=mean, stddev=stddev,
                                          name=w_name))
    return tf.add (tf.matmul (in_data, w), b, name=name)


def calculate_metrics (tp, tn, fp, fn, sig=None) :
    """
    Given the results of a test, calculate accuracy, precision, recall and
    F1 score.

    Parameters
    ----------
    tp: int
        True positives (label == prediction == 1)
    tn: int
        True negatives (label == prediction == 0)
    fp: int
        False positives (label == 0 ; prediction == 1)
    fn: int
        False negatives (label == 1 ; prediction == 0)
    sig: int, optional
        Will round the returned results to the closer value with `sig` decimal
        numbers. If `sig == None` the result won't be round.
        By default, `sig` worth `None`

    Returns
    -------
    Four floats: the accuracy, the precision, the recall and the F1 score.
    """
    full = tp + tn + fp + fn
    if full == 0 :
        acc = None
    else :
        acc = (tp + tn) / full
        if not sig is None :
            acc = round (acc, sig)

    pos = tp + fp
    if pos == 0 :
        pre = None
    else :
        pre = tp / pos
        if not sig is None :
            pre = round (pre, sig)

    one = tp + fn
    if one == 0 :
        rec = None
    else :
        rec = tp / one
        if not sig is None :
            rec = round (rec, sig)

    if pre is None or rec is None or pre + rec == 0 :
        fs = None
    else :
        fs = 2 * pre * rec / (pre + rec)
        if not sig is None :
            fs = round (fs, sig)

    return acc, pre, rec, fs


def init_saver (name) :
    """
    Create a saver object

    Parameters
    ----------
    name : string
        The name of the network to save (the one given to build_cnn)

    Returns 
    -------
    A tf.train.Saver
    """
    tf.add_to_collection ("network", name+"_C1_conv_W")
    return tf.train.Saver ()


def build_cnn (in_data,
               c1_nb_filters, c1_window_shape, p1,
               c2_nb_filters, c2_window_shape, p2,
               nb_hidden, nb_output, name) :
    """
    Build a whole CNN with 2 CONV followed by 2 FC

    Parameters
    ----------
    in_data : 3 or 4-tensor ([batch,] heigh, width, channels)
        The input data (X or previous layer's output)
        A 3-tensor will be treated as a 4-tensor, with dimension `batch` is 1
    c1_nb_filters : int
        the number of filters in // in the first CONV layer
    c1_window_shape : [int, int]
        The dimensions of the slidding windows in the first CONV layer
    p1 : [int, int]
        The dimensions of the pooling windows in the first CONV layer
    c2_nb_filters : int
        the number of filters in // in the second CONV layer
    c2_window_shape : [int, int]
        The dimensions of the slidding windows in the second CONV layer
    p2 : [int, int]
        The dimensions of the pooling windows in the second CONV layer
    nb_hidden : int
        the number of percepton in the first fully connected layer
    nb_output : int
        the number of outputs
    name : string
        Name of the network, used as a prefix to name all the TensorFlow object
        created to make the network.

    Returns
    -------
    A TensorFlow operation that perform a cnn stuff.
    """
    c1 = conv_relu_pool_layer (in_data, c1_nb_filters, c1_window_shape, p1,
                               name + "_C1")
    c2 = conv_relu_pool_layer (c1, c2_nb_filters, c2_window_shape, p2,
                               name + "_C2")
    flattened = tf.reshape (c2, [-1, c2_nb_filters * 8 * 8])
    f1 = tf.nn.relu (fc_layer (flattened, nb_hidden))
    return fc_layer (f1, nb_output)
    

def main (args) :
    batch_size = 32  # I've no gpu and size vary, so ... dunno
    # what about momentum ?... look if it's contained inside something else ...

    # Load data
    x = tf.placeholder (tf.float32, [None, 32, 32, 3], name="x")
    y = tf.placeholder (tf.float32, [None, 1], name="y")

    # Create the network topology
    logits = build_cnn (x, 16, [5,5], [2,2], 512, [7,7], [2,2], 20, 1, "cnn")
    threshold = tf.placeholder (tf.float32, [None, 1], name="threshold")
    prediction = tf.cast (tf.equal (threshold,
                                    tf.minimum (logits,
                                                threshold)),
                          tf.float32)

    # Define a loss function
    # error = tf.reduce_mean (tf.square (tf.subtract (logits, y)))
    error = tf.reduce_sum (tf.nn.sigmoid_cross_entropy_with_logits (
        labels=y, logits=logits))

    # Create an optimizer
    optimizer = tf.train.AdamOptimizer (learning_rate=args.eta) \
                        .minimize (error)
    # optimizer = tf.train.GradientDescentOptimizer (
    #     learning_rate=args.eta).minimize (error)

    # For accuracy amd other measures computation.
    pred_is_ok = tf.equal (prediction, y)
    
    # Run the session
    init_op = tf.global_variables_initializer ()
    with tf.Session () as session :
        session.run (init_op)

        # Load data
        train_data = sio.loadmat (TRAIN_FILE)
        train_images = train_data["X"]
        train_labels = train_data["y"]
        test_data = sio.loadmat (TEST_FILE)
        test_images = test_data["X"]
        test_labels = test_data["y"]

        # Calculate setup parameters
        train_size = train_labels.shape[0]
        test_size  = test_labels.shape[0]
        nb_train = min (args.nb_train, train_size)
        nb_test  = min (args.nb_test,  test_size)
        nb_train_batch = int (np.ceil (nb_train / batch_size))
        # nb_test_batch  = int (np.ceil (nb_test  / batch_size))
        nb_train = nb_train_batch * batch_size
        # nb_test  = nb_test_batch  * batch_size

        # state saver
        best_f_score = 0.
        saver = init_saver ("cnn")

        # Launch one run
        for i in range (args.iterations) :
            avg_err = 0
            
            # Train
            for j in range (nb_train_batch) :
                X = []
                Y = []
                for k in range (batch_size) :
                    rand = np.random.randint (train_size)
                    norm_img = contrast_normalization (
                        train_images[:, :, :, rand], 3)
                    X.append (norm_img)                    
                    digit = train_labels[rand, 0]
                    Y.append ([int (digit == args.digit)])
                    
                _, err = session.run ([optimizer, error],
                                      feed_dict={x: X, y: Y})
                avg_err += err / nb_train_batch

                if verbose and j % 5 == 0 :
                    util.clear_line ()
                    util.print_percent (j / nb_train_batch,
                                        "Iteration nb {} training:".format (i))

            # Test
            # avg_accuracy = 0
            true_positives  = 0
            true_negatives  = 0
            false_positives = 0
            false_negatives = 0
            for j in range (nb_test) :
                # test one matrix
                norm_img = contrast_normalization (test_images[:, :, :, j], 3)
                matrix = [norm_img]
                digit = test_labels[j, 0]
                label = [[int (digit == args.digit)]]
                
                correct, pred, lo = session.run ([pred_is_ok, prediction, logits],
                                       feed_dict={
                                           x: matrix, y: label,
                                           threshold: [[args.threshold]]})

                print ("coorect, pred, lo ? ", correct, pred, lo)

                true_positives  += label[0][0]           * int (correct)
                true_negatives  += int (not label[0][0]) * int (correct)
                false_positives += int (not label[0][0]) * int (not correct)
                false_negatives += label[0][0]           * int (not correct)

                if verbose and j % (5 * batch_size) == 0 :
                    util.clear_line ()
                    util.print_percent (j / nb_test,
                                        "Iteration nb {} testing:".format (i))

            accuracy, precision, recall, f_score = calculate_metrics (
                true_positives, true_negatives,
                false_positives, false_negatives, 3)
            util.clear_line ()
            print (("Iteration nb {} : "
                    + "err = {:.3f}\t"
                    # + "acc = {}\t "
                    + "pre = {}\t "
                    + "rec = {}\t "
                    + "F1s = {}")
                   .format (i, avg_err,  # accuracy,
                            precision, recall, f_score))

            if (not f_score is None) and (f_score > best_f_score) :
                saver.save (session, "../models/mon_model")
                
        restorer = tf.train.import_meta_graph ("../models/mon_model.meta")
        restorer.restore (session, tf.train.latest_checkpoint ("../models"))

        # Use it on non-cropped pictures
        while True :
            input_str = input (
                "Enter the \"full\" or 'cropped' followed by the number of the"
                + "\npicture to analyse, or \"exit\" : ")
            if input_str in ["exit", 'e', "quit", 'q', 'X', 'x', '', ''] :
                break  # end of analyse

            try :
                validation, img_num_str = input_str.split ()
                image_number = int (img_num_str)
            except :
                print ("Invalid syntax, input was not 'full/cropped' and"
                       +" integer, neither the string `exit`")
                continue

            if validation == "full" :
                try :
                    image = open_test_png (image_number)
                except :
                    print ("file not found")
                    continue
                
                width, heigh = image.size
                # todo : batcher tous les patchs : c'est relou en fait
                for i in range (0, width - 31, args.slidding) :
                    for j in range (0, heigh - 31, args.slidding) :
                        patch = [ [ image.getpixel ((x,y))       \
                                    if x < width and y < heigh   \
                                    else (0,0,0)                 \
                                    for x in range (i, i + 32) ] \
                                  for y in range (j, j + 32) ]
                        patch = contrast_normalization (np.array (patch), 3)
                        classes = session.run (
                            prediction,
                            feed_dict={x:[patch],
                                       threshold:[[args.threshold]]})
                        if classes == 1 :
                            max_i = min (i + 32, width - 1)
                            max_j = min (j + 32, heigh - 1)
                            for ii in range (i, max_i + 1) :
                                image.putpixel ((ii,    j), (255,0,0))
                                image.putpixel ((ii,max_j), (255,0,0))
                            for jj in range (j, max_j + 1) :
                                image.putpixel ((i,    jj), (255,0,0))
                                image.putpixel ((max_i,jj), (255,0,0))
                image.show ()

            elif validation == "cropped" :
                image = test_images[:,:,:,image_number]
                pred = session.run (prediction,
                                    feed_dict={x:[image],
                                               threshold:[[args.threshold]]})
                print ("Is" + ("" if pred == 1 else "n't") + " a "
                       + str (args.digit))
                img = Image.fromarray (np.uint8 (image))
                img.show ()

            
if __name__ == "__main__" :
    print ("TODOs are written in the source code")
    print ("###")
    print ("### STARTING - {}".format (sys.argv[0]))
    print ("###")

    parser = argparse.ArgumentParser ()
    parser.add_argument ("-η", "--eta", "--learning-rate", default=.0001,
                         type=float, help="The perceptrons' learning rate",
                         dest="eta")
    parser.add_argument ("-n", "--nb-iterations", default=12, type=int,
                         help="How many learning iterations on the dataset",
                         dest="iterations")
    parser.add_argument ("-s", "--threshold", default=.5, type=float,
                         help="Minimum probability to consider a digit as "
                         +"present", dest="threshold")
    parser.add_argument ("-T", "--nb-train", default=73257, type=int,
                         help="How many images used for training. Maximum is "
                         +"73257", dest="nb_train")
    parser.add_argument ("-t", "--nb-test", default=26032, type=int,
                         help="How many images used for testing. Maximum is "
                         +"26032", dest="nb_test")
    parser.add_argument ("-w", "--slidding-window", default=16, type=int,
                         help="How many pixel should the 32x32 slidding window "
                         +"move at each iteration", dest="slidding")
    parser.add_argument ("-y", "--number-to-localize", default=3, type=int,
                         help="Which digit should the network learn to "
                         + "identify", dest="digit")
                             
    main (parser.parse_args ())
    
